const Sequelize = require('sequelize')
const _ = require('lodash')
const mime = require('mime-types')
const path = require('path')
const siteStatus = _.map(require('../data/sd/status/site.json').fixture, 'id')
const { createError, fs, fg, toUnixPath } = require('../lib/helper')
const { upload } = require('../lib/context')

const schema = {
  status: Sequelize.STRING(20),
  domain: { type: Sequelize.STRING(100), unique: true },
  title: Sequelize.STRING(50),
  organization: Sequelize.STRING(100),
  address1: Sequelize.STRING,
  address2: Sequelize.STRING,
  city: Sequelize.STRING(50),
  zipCode: Sequelize.STRING(10),
  state: Sequelize.STRING(50),
  country: Sequelize.STRING(2),
  phone: Sequelize.STRING(50),
  email: Sequelize.STRING(100),
  website: Sequelize.STRING,
  builtIn: { type: Sequelize.BOOLEAN, defaultValue: false }
}

const fields = _.concat(
  ['id', 'createdAt', 'updatedAt'],
  _.keys(schema)
)

const omitted = ['id', 'status', 'builtIn']

const entityValidator = {
  domain: { type: 'string', min: 5, max: 20, empty: false, trim: true, lowercase: true, unique: true },
  status: { type: 'enum', values: siteStatus },
  organization: { type: 'string', trim: true },
  address1: { type: 'string', trim: true },
  address2: { type: 'string', trim: true },
  city: { type: 'string', empty: false, max: 50, trim: true },
  state: { type: 'string', empty: false, max: 50, trim: true },
  zipCode: { type: 'string', max: 10, trim: true },
  country: { type: 'string', empty: false, length: 2, uppercase: true },
  phone: { type: 'string', trim: true },
  email: { type: 'email', normalize: true, trim: true }
}

module.exports = ({ dbSql }) => {
  return {
    mixins: [dbSql],
    model: {
      define: schema,
      options: {
        underscored: true,
        indexes: [
          { fields: ['status'] },
          { fields: ['organization'] },
          { fields: ['city'] },
          { fields: ['country'] },
          { fields: ['created_at'] },
          { fields: ['updated_at'] }
        ]
      }
    },
    dependencies: ['coreUser'],
    settings: {
      fields,
      entityValidator,
      broadcastRecord: true,
      webApi: { disabled: 'all' },
      webRoot: { anonymous: ['getLogo', 'getSite'] }
    },
    actions: {
      async getSite (ctx) {
        if (!ctx.meta.site) throw createError('Site unavailable', 500)
        return _.omit(ctx.meta.site, omitted)
      },
      async updateSite (ctx) {
        if (!ctx.meta.site) throw createError('Site unavailable', 500)
        const params = _.omit(ctx.params, ['id', 'domain', 'status'])
        params.id = ctx.meta.site.id
        const site = await ctx.call('coreSite.update', params)
        return _.omit(site || {}, omitted)
      },
      async getLogo (ctx) {
        return this.getLogo(ctx, ctx.meta.site.id)
      },
      removeLogo (ctx) {
        return new Promise((resolve, reject) => {
          if (!ctx.meta.user) throw createError('Unauthenticated', 401)
          fg(this.getLogoDir() + '/' + ctx.meta.site.id + '.*')
            .then(files => {
              if (files.length === 0) throw createError('File not found', 404)
              _.each(files, f => {
                try {
                  fs.unlinkSync(files[0])
                } catch (err) {}
              })
              resolve({ msg: 'Logo removed' })
            })
            .catch(reject)
        })
      },
      uploadLogo (ctx) {
        if (!ctx.meta.user) throw createError('Unauthenticated', 401)
        return upload(ctx, {
          destDir: this.getLogoDir(),
          fname: ctx.meta.site.id
        })
      }
    },
    hooks: {
      after: {
        create: ['createUser'],
        apiCreate: ['createUser']
      }
    },
    methods: {
      async createUser (ctx, data) {
        let fixtures = []
        try {
          fixtures = require(`../data/template/user.json`)
        } catch (err) {}
        if (fixtures.length === 0) return data
        for (let i = 0; i < fixtures.length; i++) {
          fixtures[i].siteId = data.id
          fixtures[i].email = f.email.replace('{domain}', data.domain)
          if (!data.domain.includes('.')) fixtures[i].email += '.localdomain'
          try {
            await this.broker.call('coreUser.create', fixtures[i])
          } catch (err) {}
        }
        return data
      },
      getLogoDir () {
        return toUnixPath(path.join(this.broker.sobr.dataDir, 'upload', 'coreSite', 'logo'))
      },
      getLogo (ctx, id) {
        return new Promise((resolve, reject) => {
          fg(this.getLogoDir() + '/' + id + '.*')
            .then(files => {
              if (files.length === 0) throw createError('File not found', 404)
              ctx.meta.$responseType = mime.lookup(path.basename(files[0]))
              const f = fs.createReadStream(files[0])
              f.on('error', err => {
                throw err
              })
              resolve(f)
            })
            .catch(reject)
        })
      }
    }
  }
}
