const Sequelize = require('sequelize')
const _ = require('lodash')
const { resolveFixture } = require('../../lib/helper')

const schema = {
  id: { type: Sequelize.STRING(2), primaryKey: true },
  name: { type: Sequelize.STRING },
  iso3: { type: Sequelize.STRING(3) },
  phoneCode: { type: Sequelize.STRING(20) },
  capital: { type: Sequelize.STRING(50) },
  currency: { type: Sequelize.STRING(20) },
  mmsi: { type: Sequelize.STRING(50) },
  ioc: { type: Sequelize.STRING(10) },
  bbox: { type: Sequelize.STRING }
}

const fields = _.keys(schema)

module.exports = ({ sob, sd }) => {
  return {
    mixins: [sd],
    model: {
      define: schema,
      options: {
        underscored: true
      }
    },
    settings: {
      fields,
      defFields: ['id', 'name'],
      webApi: { readonly: true, anonymous: ['get', 'list'] },
      webStream: { anonymous: true }
    },
    afterStreamAttached () {
      const executor = async data => {
        await this.adapter.model.bulkCreate(data)
      }
      resolveFixture(sob, this.broker.sobr, executor)
    }
  }
}
