const Sequelize = require('sequelize')
const { createError, paginateArray } = require('../../lib/helper')
const _ = require('lodash')

module.exports = ({ sob }) => {
  const pagination = {
    default: _.get(sob, 'config.web.api.pagination.default', 10),
    max: _.get(sob, 'config.web.api.pagination.max', 50)
  }
  return {
    settings: {
      pageSize: pagination.default,
      maxPageSize: pagination.max
    },
    actions: {
      async groupBy (ctx) {
        if (_.isEmpty(ctx.params.entity)) throw createError('Entity not provided', 500)
        if (_.isEmpty(ctx.params.field)) throw createError('Field not provided', 500)
        const svcName = _.camelCase(_.kebabCase(ctx.params.entity))
        const svc = _.find(this.broker.services, { name: svcName })
        if (!svc) throw createError('Invalid entity', 500)
        let limit = ctx.params.pageSize || this.settings.pageSize
        if (limit > this.settings.maxPageSize) limit = this.settings.maxPageSize
        const page = parseInt(ctx.params.page) || 1
        const records = await svc.adapter.model.findAll({
          attributes: [[ctx.params.field, 'id'], [Sequelize.fn('count', Sequelize.col(ctx.params.field)), 'count']],
          group: [ctx.params.field],
          order: [[Sequelize.col('count'), 'DESC']],
          raw: true
        })
        const results = paginateArray(records, page, limit)
        return { rows: results.data, page: results.currentPage, pageSize: results.perPage, total: results.total, totalPages: results.totalPages }
      }
    }
  }
}
