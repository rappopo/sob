const { createError, DateTime } = require('../../lib/helper')
const _ = require('lodash')
const sjs = require('sequelize-json-schema')

module.exports = () => {
  return {
    actions: {
      async meta (ctx) {
        if (_.isEmpty(ctx.params.entity)) throw createError('Entity not provided', 500)
        const svcName = _.camelCase(_.kebabCase(ctx.params.entity))
        const svc = _.find(this.broker.services, { name: svcName })
        if (!svc) throw createError('Invalid entity', 500)
        if (!svc.schema.model) throw createError('Invalid entity', 500)
        const data = sjs.getModelSchema(svc.adapter.model)
        const index = _.get(svc.schema, 'model.options.indexes', [])
        const schema = []
        _.forOwn(data.properties, (v, k) => {
          if (['siteId'].includes(k)) return
          const name = k
          let type = _.isArray(v.type) ? v.type[0] : v.type
          if (v.format === 'date-time') type = 'datetime'
          if (v.format === 'date') type = 'date'
          if (v.format === 'double') type = 'float'
          let searchable = _.get(svc.schema, 'model.define.' + k + '.unique')
          _.each(index, idx => {
            const key = _.get(svc.schema, 'model.options.underscored') ? _.snakeCase(k) : k
            if ((idx.fields || []).includes(key)) searchable = true
          })
          if (['id', 'siteId'].includes(k)) searchable = true
          const validator = _.get(svc.schema.settings, 'validatorSchema.' + k, {})
          const minLength = validator.min || 0
          const maxLength = validator.max || v.maxLength || 0
          let required = (_.without(data.required, 'createdAt', 'updatedAt') || []).includes(k)
          if (validator.type === 'string' && validator.empty === false) required = true
          schema.push({ name, type, minLength, maxLength, required, searchable, validator })
        })
        return {
          ts: DateTime.utc().toISO(),
          schema
        }
      }
    }
  }
}
