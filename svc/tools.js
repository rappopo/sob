const { _, createError, hash, DateTime } = require('../lib/helper')

module.exports = () => {
  return {
    settings: {
      webApi: {
        alias: {
          'GET /md5': 'md5'
        }
      }
    },
    actions: {
      md5 (ctx) {
        if (_.isEmpty(ctx.params.text)) throw createError('Text not provided', 400)
        return {
          ts: DateTime.utc().toISO(),
          result: hash(ctx.params.text)
        }
      }
    }
  }
}
