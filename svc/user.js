const Sequelize = require('sequelize')
const path = require('path')
// const S = require('string')
const _ = require('lodash')
const bcrypt = require('bcrypt')
const crypto = require('crypto')
const jose = require('jose')
const mime = require('mime-types')
const { DateTime } = require('luxon')
const userStatus = _.map(require('../data/sd/status/user.json').fixture, 'id')
const { createError, fs, fg, toUnixPath, hash } = require('../lib/helper')
const { upload } = require('../lib/context')

const schema = {
  siteId: { type: Sequelize.INTEGER, defaultValue: 0 },
  status: Sequelize.STRING(20),
  username: Sequelize.STRING(20),
  token: Sequelize.STRING(100),
  password: Sequelize.STRING(100),
  keyX: Sequelize.STRING,
  keyD: Sequelize.STRING,
  firstName: Sequelize.STRING(50),
  lastName: Sequelize.STRING(50),
  address1: Sequelize.STRING,
  address2: Sequelize.STRING,
  city: Sequelize.STRING(50),
  zipCode: Sequelize.STRING(10),
  state: Sequelize.STRING(50),
  country: Sequelize.STRING(2),
  phone: Sequelize.STRING(50),
  email: Sequelize.STRING(100),
  builtIn: { type: Sequelize.BOOLEAN, defaultValue: false }
}

const fields = _.concat(
  ['id', 'createdAt', 'updatedAt'],
  _.keys(schema)
)

const entityValidator = {
  username: { type: 'string', min: 5, max: 20, empty: false, trim: true, lowercase: true, alphanum: true },
  password: { type: 'string', min: 5, empty: false },
  email: { type: 'email', normalize: true, trim: true },
  firstName: { type: 'string', max: 50, trim: true },
  lastName: { type: 'string', max: 50, trim: true },
  address1: { type: 'string', optional: true, trim: true },
  address2: { type: 'string', optional: true, trim: true },
  city: { type: 'string', optional: true, max: 50, trim: true },
  state: { type: 'string', optional: true, empty: false, max: 50, trim: true },
  zipCode: { type: 'string', optional: true, max: 10, trim: true },
  country: { type: 'string', optional: true, length: 2, uppercase: true },
  phone: { type: 'string', optional: true, trim: true },
  status: { type: 'enum', optional: true, values: userStatus }
}
const beforeCreate = ['hashPassword']
const beforeUpdate = [
  'hashPassword',
  function checks (ctx) {
    ctx.params = _.omit(ctx.params, ['username'])
  }
]

module.exports = ({ sob, dbSql }) => {
  const config = sob.config
  return {
    mixins: [dbSql],
    model: {
      define: schema,
      options: {
        underscored: true,
        indexes: [
          { unique: true, fields: ['site_id', 'username'] },
          { unique: true, fields: ['site_id', 'email'] },
          { fields: ['token'] },
          { fields: ['status'] },
          { fields: ['first_name'] },
          { fields: ['last_name'] },
          { fields: ['city'] },
          { fields: ['country'] },
          { fields: ['created_at'] },
          { fields: ['updated_at'] }
        ]
      }
    },
    settings: {
      fields,
      entityValidator,
      broadcastRecord: true,
      webApi: { disabled: 'all', anonymous: ['createJwt', 'getAvatar', 'register', 'activate'] }
    },
    actions: {
      async getBearer (ctx) {
        if (!ctx.meta.user) throw createError('Unauthenticated', 401)
        return _.pick(ctx.meta.user, ['updatedAt', 'token'])
      },
      createBearer: {
        params: {
          password: { type: 'string', empty: false }
        },
        async handler (ctx) {
          if (!ctx.meta.user) throw createError('Unauthenticated', 401)
          const compare = await bcrypt.compare(ctx.params.password, ctx.meta.user.password)
          if (!compare) throw createError('Current password is wrong', 400, { password: 'Wrong password' })
          const user = await ctx.call('coreUser.apiUpdate', {
            id: ctx.meta.user.id,
            username: ctx.meta.user.username,
            password: ctx.params.password,
            _sysForce: true
          })
          return _.merge(_.pick(user, ['updatedAt', 'token']), { msg: 'Bearer token successfully updated' })
        }
      },
      createJwt: {
        params: {
          username: { type: 'string', min: 5, empty: false },
          password: { type: 'string', empty: false }
        },
        async handler (ctx) {
          const user = await this.getUser(ctx)
          const key = jose.JWK.asKey({
            crv: 'Ed25519',
            kty: 'OKP',
            x: user.keyX,
            d: user.keyD,
            kid: user.token
          })
          const token = jose.JWT.sign({ uid: user.id }, key, config.web.api.jwt)
          return { created_at: DateTime.local().toJSDate(), token }
        }
      },
      async verifyJwt (ctx) {
        const decoded = jose.JWT.decode(ctx.params.token, { complete: true })
        const user = await ctx.call('coreUser.get', { id: decoded.payload.uid })
        const key = jose.JWK.asKey({
          crv: 'Ed25519',
          kty: 'OKP',
          x: user.keyX,
          d: user.keyD,
          kid: user.token
        })
        jose.JWT.verify(ctx.params.token, key)
        return decoded.header.kid === user.token ? user : null
      },
      async verifyBearer (ctx) {
        const users = await ctx.call('coreUser.find', { query: { token: ctx.params.token } })
        if (users.length === 0) throw createError('Invalid token', 400, { token: 'Invalid token' })
        if (users[0].status !== 'ENABLED') throw createError('User is disabled', 400, { token: 'User is disabled' })
        return users[0]
      },
      async verifyBasic (ctx) {
        const users = await ctx.call('coreUser.find', { query: { username: ctx.params.username } })
        if (users.length === 0) throw createError('User not found', 400, { token: 'User not found' })
        if (users[0].status !== 'ENABLED') throw createError('User is disabled', 400, { username: 'User is disabled' })
        const compare = await bcrypt.compare(ctx.params.password, users[0].password)
        return compare ? users[0] : null
      },
      register: {
        // TODO: send email
        params: {
          username: { type: 'string', min: 5, empty: false, trim: true },
          password: { type: 'string', empty: false },
          firstName: { type: 'string', empty: false, trim: true },
          lastName: { type: 'string', empty: false, trim: true },
          email: { type: 'email', normalize: true, trim: true, empty: false }
        },
        async handler (ctx) {
          ctx.params.status = 'UNVERIFIED'
          const users = await ctx.call('coreUser.find', {
            query: {
              $or: [{
                username: ctx.params.username,
                siteId: ctx.meta.site.id
              }, {
                email: ctx.params.email,
                siteId: ctx.meta.site.id
              }]
            }
          })
          if (users.length > 0) {
            const errors = {}
            _.each(users, u => {
              if (u.username === ctx.params.username) errors.username = 'Already taken'
              if (u.email === ctx.params.email) errors.email = 'Already used'
            })
            if (!_.isEmpty(errors)) throw createError('Username/email already taken/used', 400, errors)
          }
          const user = await ctx.call('coreUser.apiCreate', ctx.params)
          return {
            ts: user.createdAt,
            siteId: user.siteId,
            username: user.username,
            status: user.status,
            activationKey: hash(`${user.keyX}|${user.keyD}`),
            msg: 'Verification in progress'
          }
        }
      },
      activate: {
        // TODO: send email
        params: {
          username: { type: 'string', min: 5, empty: false, trim: true },
          key: { type: 'string', empty: false }
        },
        async handler (ctx) {
          const users = await ctx.call('coreUser.find', {
            query: { username: ctx.params.username }
          })
          if (users.length === 0) throw createError('Unknown user', 400, { username: 'Unknown username' })
          if (users[0].status !== 'UNVERIFIED') throw createError('User is already verififed', 400, { username: 'Verified already' })
          if (hash(`${users[0].keyX}|${users[0].keyD}`) !== ctx.params.key) throw createError('Invalid activation key', 400, { key: 'Invalid' })
          try {
            const user = await ctx.call('coreUser.apiUpdate', {
              id: users[0].id,
              status: 'ENABLED',
              _sysForce: true
            })
            return {
              updatedAt: user.updatedAt,
              siteId: user.siteId,
              username: user.username,
              status: user.status,
              msg: 'Verification successfull'
            }
          } catch (err) {
            console.log(err)
          }
        }
      },
      async getMyProfile (ctx) {
        if (!ctx.meta.user) throw createError('Unauthenticated', 401)
        return _.omit(ctx.meta.user, ['keyX', 'keyD', 'password'])
      },
      async getMyRole (ctx) {
        if (!ctx.meta.user) throw createError('Unauthenticated', 401)
        return _.omit(ctx.meta.role || {}, ['editUserprop', 'builtIn', 'filterCol'])
      },
      async updateMyProfile (ctx) {
        if (!ctx.meta.user) throw createError('Unauthenticated', 401)
        const params = _.omit(ctx.params, ['id', 'password', 'username', 'token', 'keyX', 'keyD', 'status'])
        params.id = ctx.meta.user.id
        params._sysForce = true
        const user = await ctx.call('coreUser.apiUpdate', params)
        return _.omit(user || {}, ['keyX', 'keyD', 'password'])
      },
      changePassword: {
        params: { old: entityValidator.password, new: entityValidator.password },
        async handler (ctx) {
          if (!ctx.meta.user) throw createError('Unauthenticated', 401)
          const compare = await bcrypt.compare(ctx.params.old, ctx.meta.user.password)
          if (!compare) throw createError('Current password is wrong', 400, { old: 'Wrong password' })
          const user = await ctx.call('coreUser.apiUpdate', {
            id: ctx.meta.user.id,
            username: ctx.meta.user.username,
            password: ctx.params.new,
            _sysForce: true
          })
          return _.merge(_.pick(user, ['updatedAt']), { msg: 'Password successfully updated' })
        }
      },
      async getMyAvatar (ctx) {
        return this.getAvatar(ctx, ctx.meta.user.id)
      },
      getAvatar: {
        params: {
          id: { type: 'string', empty: false }
        },
        handler: async function (ctx) {
          return this.getAvatar(ctx, ctx.params.id)
        }
      },
      removeAvatar (ctx) {
        return new Promise((resolve, reject) => {
          if (!ctx.meta.user) throw createError('Unauthenticated', 401)
          fg(this.getAvatarDir() + '/' + ctx.meta.user.id + '.*')
            .then(files => {
              if (files.length === 0) throw createError('File not found', 404)
              _.each(files, f => {
                try {
                  fs.unlinkSync(files[0])
                } catch (err) {}
              })
              resolve({ msg: 'Avatar removed' })
            })
            .catch(reject)
        })
      },
      uploadAvatar (ctx) {
        if (!ctx.meta.user) throw createError('Unauthenticated', 401)
        return upload(ctx, {
          destDir: this.getAvatarDir(),
          fname: ctx.meta.user.id
        })
      }
    },
    hooks: {
      before: {
        create: beforeCreate,
        apiCreate: beforeCreate,
        update: beforeUpdate,
        apiUpdate: beforeUpdate
      }
    },
    methods: {
      /*
      async createKey (params) {
        const key = await jose.JWK.generate('OKP', 'Ed25519', { kid: params.secret })
        return key.toJWK(true)
      },
      */
      getAvatarDir () {
        return toUnixPath(path.join(this.broker.sobr.dataDir, 'upload', 'coreUser', 'avatar'))
      },
      getAvatar (ctx, id) {
        return new Promise((resolve, reject) => {
          fg(this.getAvatarDir() + '/' + id + '.*')
            .then(files => {
              if (files.length === 0) throw createError('File not found', 404)
              ctx.meta.$responseType = mime.lookup(path.basename(files[0]))
              const f = fs.createReadStream(files[0])
              f.on('error', err => {
                throw err
              })
              resolve(f)
            })
            .catch(reject)
        })
      },
      async hashPassword (ctx) {
        if (_.isEmpty(ctx.params.password)) return ctx
        const hashed = await bcrypt.hash(ctx.params.password, 10)
        const token = crypto.createHash('md5').update(`${ctx.params.username}:${hashed}`).digest('hex')
        ctx.params.password = hashed
        ctx.params.token = token
        const result = await jose.JWK.generate('OKP', 'Ed25519', { kid: token })
        const key = result.toJWK(true)
        ctx.params.keyX = key.x
        ctx.params.keyD = key.d
      },
      async getUser (ctx) {
        const users = await ctx.call('coreUser.find', {
          query: {
            username: ctx.params.username
          }
        })
        if (users.length === 0) throw createError('User not found', 400, { username: 'Unknown user' })
        if (users[0].status !== 'ENABLED') throw createError('User is disabled', 400, { username: 'User is disabled' })
        const result = await bcrypt.compare(ctx.params.password, users[0].password)
        if (!result) throw createError('Invalid password', 400, { password: 'Wrong password' })
        return users[0]
      }
    }
  }
}
