const { _, createError, toUnixPath, fg, fs, mime } = require('../lib/helper')
const context = require('../lib/context')
const path = require('path')
const types = ['image', 'document']

module.exports = ({ sobr, sob }) => {
  return {
    settings: {
      webApi: {
        alias: {
          'GET /:entity/:entityId/:type': 'list',
          'GET /:entity/:entityId/:type/:file': 'get',
          'PUT /:entity/:entityId/:type/:file': 'rename',
          'DELETE /:entity/:entityId/:type/:file': 'remove'
        }
      },
      webRoot: {
        // anonymous: ['stream'],
        alias: {
          'GET /:entity/:entityId/:type/:file': 'stream'
        }
      },
      upload: {
        /*
        alias: {
          '/:entity/:id': 'uploadAttachment'
        }
        params: {
          busboyConfig: { limits: { files: 1 } }
        }
        */
      }
    },
    actions: {
      upload (ctx) {
        return new Promise((resolve, reject) => {
          const type = ctx.meta.$multipart.type || ''
          const entity = ctx.meta.$multipart.entity || ''
          const entityId = ctx.meta.$multipart.entityId || ''
          const fname = ctx.meta.filename === 'blob' ? ctx.meta.fieldname : ctx.meta.filename
          // fname = fname.replace(/\s+/g, '_').toLowerCase()
          let svcName
          Promise.resolve()
            .then(() => {
              svcName = this.checkParams(ctx, { type, entity, entityId })
              return ctx.broker.call(`${svcName}.find`, { query: { id: entityId }, limit: 1 })
            })
            .then(result => {
              if (result.length === 0) throw createError('Entity not found', 404, { id: 'notFound' })
              return context.upload(ctx, {
                destDir: this.getDir(svcName, entityId, type),
                fname
              })
            })
            .then(resolve)
            .catch(reject)
        })
      },
      stream (ctx) {
        return new Promise((resolve, reject) => {
          const { type, entity, entityId, file } = ctx.params
          let svcName
          Promise.resolve()
            .then(() => {
              svcName = this.checkParams(ctx, { type, entity, entityId, file }, true)
              return fg(this.getDir(svcName, entityId, type) + '/' + file)
            })
            .then(files => {
              if (files.length === 0) throw createError('File not found', 404)
              ctx.meta.$responseType = mime.lookup(path.basename(files[0]))
              const f = fs.createReadStream(files[0])
              f.on('error', err => {
                throw err
              })
              resolve(f)
            })
            .catch(reject)
        })
      },
      get (ctx) {
        return new Promise((resolve, reject) => {
          const { type, entity, entityId, file } = ctx.params
          let svcName
          Promise.resolve()
            .then(() => {
              svcName = this.checkParams(ctx, { type, entity, entityId, file }, true)
              return fg(this.getDir(svcName, entityId, type) + '/' + file)
            })
            .then(files => {
              if (files.length === 0) throw createError('File not found', 404)
              resolve(this.fileInfo(files[0]))
            })
            .catch(reject)
        })
      },
      list (ctx) {
        return new Promise((resolve, reject) => {
          const { type, entity, entityId } = ctx.params
          let svcName
          Promise.resolve()
            .then(() => {
              svcName = this.checkParams(ctx, { type, entity, entityId })
              return fg(this.getDir(svcName, entityId, type) + '/*')
            })
            .then(files => {
              // TODO: paginate
              const result = {
                page: 1,
                pageSize: files.length,
                total: files.length
              }
              result.rows = _.map(files, f => this.fileInfo(f))
              resolve(result)
            })
            .catch(reject)
        })
      },
      remove (ctx) {
        return new Promise((resolve, reject) => {
          const { type, entity, entityId, file } = ctx.params
          let svcName
          Promise.resolve()
            .then(() => {
              svcName = this.checkParams(ctx, { type, entity, entityId, file }, true)
              return fg(this.getDir(svcName, entityId, type) + '/' + file)
            })
            .then(files => {
              if (files.length === 0) throw createError('File not found', 404)
              _.each(files, f => {
                try {
                  fs.unlinkSync(files[0])
                } catch (err) {}
              })
              resolve({ msg: 'Attachment removed' })
            })
            .catch(reject)
        })
      },
      rename (ctx) {
        return new Promise((resolve, reject) => {
          const { type, entity, entityId, file } = ctx.params
          let svcName
          let dir
          Promise.resolve()
            .then(() => {
              if (_.isEmpty(ctx.params.name)) throw createError('New name required', 400, { name: 'required' })
              svcName = this.checkParams(ctx, { type, entity, entityId, file }, true)
              dir = this.getDir(svcName, entityId, type)
              return fg(dir + '/' + file)
            })
            .then(files => {
              if (files.length === 0) throw createError('File not found', 404)
              const ofile = path.join(dir, file)
              const nfile = path.join(dir, ctx.params.name)
              fs.renameSync(ofile, nfile)
              resolve({ msg: 'Attachment renamed' })
            })
            .catch(reject)
        })
      }
    },
    methods: {
      getDir (svcName, entityId, type) {
        return toUnixPath(path.join(this.broker.sobr.dataDir, 'upload', 'coreAttachment', svcName, entityId, type))
      },
      checkParams (ctx, { type, entity, file }, withFile) {
        if (!ctx.meta.user) throw createError('Unauthenticated', 401)
        if (!types.includes(type)) throw createError(`Only ${types.join(', ')} are supported`, 400, { type: 'Invalid type' })
        if (withFile && _.isEmpty(file)) throw createError('File is required', 400, { file: 'required' })
        const { svcName } = context.getEntity(ctx, entity)
        return svcName
      },
      fileInfo (file) {
        const stats = fs.statSync(file)
        return {
          id: path.basename(file),
          createdAt: stats.birthtime,
          updatedAt: stats.mtime,
          size: stats.size
        }
      }
    }
  }
}
