const _ = require('lodash')
const transformSort = require('../lib/transform-sort')
const context = require('../lib/context')

module.exports = ({ sob }) => {
  const maxBatchSize = _.get(sob, 'config.web.stream.maxBatchSize', 1000)
  const maxRows = _.get(sob, 'config.web.stream.maxRows', 50000)
  return {
    settings: {
      webRoot: {
        alias: {
          'GET /:entity': 'serve'
        }
      }
    },
    actions: {
      async serve (ctx) {
        const { svc, svcName } = context.getEntity(ctx)
        const where = await context.buildApiFilter(ctx, svcName)
        const format = ctx.params.format
        let batchSize = parseInt(ctx.params.batchSize || 100)
        if (batchSize > maxBatchSize) batchSize = maxBatchSize
        let limit = parseInt(ctx.params.limit || 100000)
        if (limit > maxRows) limit = maxRows
        if (limit <= 0) limit = undefined
        const order = transformSort(ctx.params.sort)
        return svc.adapter.model.findAllWithStream({ where, order, format, batchSize, limit })
      }
    },
    methods: {
      isAdmin (ctx) {
        return _.get(ctx.meta, 'role.permission.0') === '*'
      }
    }
  }
}
