module.exports = {
  actions: {
    hello (ctx) {
      return 'Hello, ' + (ctx.params.name || 'dude') + '!'
    }
  }
}
