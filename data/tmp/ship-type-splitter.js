const src = require('./sub-type-src.json')
const _ = require('lodash')
const fs = require('fs')

const type = []
let subType = []
_.each(src, s => {
  type.push({ id: s.id, name: s.name, iColor: s.icolor, aisShipType: s.ais_ship_type.join(',') })
  _.each(s.sub_type, t => {
    subType.push({ id: t, type: s.id })
  })
})

subType = _.orderBy(subType, ['id'], ['asc'])

fs.writeFileSync('./ship-type.json', JSON.stringify(type, null, 2))
fs.writeFileSync('./ship-sub-type.json', JSON.stringify(subType, null, 2))
