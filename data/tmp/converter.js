const fs = require('fs')
const src = require('./country-src.json')
const bbox = require('./country-bbox.json')
const _ = require('lodash')

_.each(src, s => {
  _.forOwn(bbox, (v, k) => {
    if (s.iso3 === k) {
      s.bbox = `${v.sw.lon},${v.sw.lat},${v.ne.lon},${v.ne.lat}`
    }
  })
  delete s.simop
})

fs.writeFileSync('./country.json', JSON.stringify(src))
