const helper = require('../lib/helper')
const { rmfr, toUnixPath, validator, mergeClone } = helper
const _ = require('lodash')
const fs = require('fs')
const util = require('util')
const readFile = util.promisify(fs.readFile)
const writeFile = util.promisify(fs.writeFile)
const path = require('path')
const copy = require('recursive-copy')
const isValidNpmName = require('is-valid-npm-name')
const { DateTime } = require('luxon')
const root = toUnixPath(process.cwd())
const loggerName = '[SobCreator]'
const logger = helper.logger({ name: loggerName })
const argv = require('minimist')(process.argv.slice(2), {
  alias: { m: 'module', f: 'force' }
})
const interactive = require('./interactive')
const ask = async q => {
  return interactive.ask(q, { loggerName })
}

let app = {}

const askAppInfo = async (options = {}) => {
  let input = {}
  do {
    input.name = await ask(`${argv.module ? 'Module' : 'Application'} name (aka. npm package name): ` + (!_.isEmpty(options.name) ? `(${options.name}) ` : ''))
    if (!_.isEmpty(input.name)) {
      const check = isValidNpmName(input.name)
      if (!_.isEmpty(check)) {
        logger.error(_.upperFirst(check))
        input.name = ''
      }
    }
  } while (_.isEmpty(input.name) && _.isEmpty(options.name))
  input.description = await ask('Description: ' + (!_.isEmpty(options.description) ? `(${options.description}) ` : ''))
  input.keywords = await ask('Keywords: ' + (!_.isEmpty(options.keywords) ? `(${options.keywords}) ` : ''))
  input.authorName = await ask('Your full name: ' + (!_.isEmpty(options.authorName) ? `(${options.authorName}) ` : ''))
  if (!_.isEmpty(input.authorName) || !_.isEmpty(options.authorName)) {
    let check = false
    do {
      input.authorEmail = await ask('Your email: ' + (!_.isEmpty(options.authorEmail) ? `(${options.authorEmail}) ` : ''))
      if (!_.isEmpty(input.authorEmail)) {
        if (validator.validate({ email: input.authorEmail }, { email: { type: 'email' } }) === true) {
          check = false
        } else {
          logger.error('Invalid email format')
          check = true
        }
      }
    } while (check)
  }
  if (argv.module) {
    do {
      input.sob = await ask('Sob name: ' + (!_.isEmpty(options.sob) ? `(${options.sob}) ` : ''))
    } while (_.isEmpty(input.sob) && _.isEmpty(options.sob))
  }
  _.forOwn(input, (v, k) => {
    input[k] = _.isEmpty(v) ? options[k] : v
  })
  let answer = ''
  while (!['y', 'n', 'c'].includes(answer)) {
    answer = await ask('Are you sure everything is correct? (y)es, continue / (n)o, edit / (c)ancel ')
    answer = answer.toLowerCase()
  }
  if (answer === 'c') return logger.info('App initialization canceled')
  if (answer === 'n') input = await askAppInfo(_.clone(input))
  return input
}

const copySkel = () => {
  return new Promise((resolve, reject) => {
    rmfr(`${root}/*`, { glob: true })
      .then(() => {
        const src = path.join(__dirname, '..', 'data', 'template', 'creator-skel', argv.module ? 'module' : 'application')
        logger.info(`Copying ${argv.module ? 'module' : 'application'} files...`)
        const options = {}
        if (argv.noSample) {
          options.filter = [
            '**/*',
            '!svc/sample/*',
            '!svc/sample',
            '!data/sd/sample/*',
            '!data/sd/sample'
          ]
        }
        return copy(src, root, options)
      })
      .then(resolve)
      .catch(reject)
  })
}

const patchFile = async (file, values) => {
  const fname = path.basename(file)
  logger.info(`Writing ${fname}...`)
  const content = await readFile(file, 'utf8')
  const result = _.template(content)(values)
  await writeFile(file, result)
}

const createAppMod = async () => {
  try {
    await copySkel()
    await patchFile(`${root}/package.json`, mergeClone(app, { sobVersion: require('../package.json').version }))
    await patchFile(`${root}/README.md`, mergeClone(app))
    await patchFile(`${root}/LICENSE.md`, mergeClone(app, { year: DateTime.local().year }))
    logger.info(`Your ${argv.module ? 'module' : 'application'} is ready`)
    logger.info(`Now its the right time to hit 'npm install'. Enjoy!`)
  } catch (err) {
    logger.error(err.message)
  }
}

module.exports = async () => {
  if (fs.readdirSync(root).length > 0) {
    let answer = ''
    while (!['y', 'n', 'yes', 'no'].includes(answer)) {
      answer = await ask('Non empty directory. Its contents need to be removed first. Continue? y/n: ')
      answer = answer.toLowerCase()
    }
    if (answer === 'n') return logger.error(`Can't continue without non empty directory`)
  }
  logger.info(`You're about to create a ${argv.module ? 'sob module' : 'sob application'} in '${path.resolve(root)}'`)
  logger.info(`Please answer the following questions first`)
  app = await askAppInfo()
  app.keywords = _.isEmpty(app.keywords) ? [] : app.keywords.replace(/\s+/g, ' ').split(' ')
  await createAppMod()
}
