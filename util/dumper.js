const argv = require('minimist')(process.argv.slice(2))
const { DateTime, mergeClone, matcherFromCli, _, persistModel } = require('../lib/helper')
const sequelizeStream = require('../lib/sequelize-stream')
const fs = require('fs')
const rmfr = require('rmfr')
const util = require('util')
const path = require('path')
const mkdir = util.promisify(fs.mkdir)
const compressing = require('compressing')
const loggerName = '[SobDumper]'
const logger = require('../lib/helper').logger({ name: loggerName })
const interactive = require('./interactive')
const ask = async q => {
  return interactive.ask(q, { loggerName })
}

class Dumper {
  constructor ({ sobr, dbs, findSob }) {
    this.sobr = sobr
    this.dbs = dbs
    this.findSob = findSob
    /*
    this.withSchema = true
    this.withData = true
    */
  }

  async run () {
    const actions = await matcherFromCli('dump', this.sobr, this.findSob)
    if (!actions) return
    /*
    if (argv.withSchema && ['n', 'no'].includes(argv.withSchema.toLowerCase())) this.withSchema = false
    if (argv.withData && ['n', 'no'].includes(argv.withData.toLowerCase())) this.withData = false
    if (!(this.withData || this.withSchema)) return logger.info(`Without schema + without data = nothing todo`)
    logger.info(`The following ${actions.length} model(s) will be exported${argv.tgz ? ' and saved as a compressed file' : '' }`)
    logger.info(`Schema:\t${this.withSchema ? 'Yes' : 'No'}`)
    logger.info(`Data:\t${this.withData ? 'Yes' : 'No'}`)
    logger.info(`Zipped:\t${argv.tgz ? 'Yes' : 'No'}`)
    logger.info('Models:')
    */
    if (!argv.quiet) {
      logger.info(`The following ${actions.length} model(s) will be exported${argv.tgz ? ' and saved as a compressed file' : ''}`)
      _.each(actions, a => {
        logger.info(`- ${a.name}`)
      })
    }
    const defName = _.isString(argv.name) ? argv.name : `${DateTime.local().toFormat('yyMMdd')}`
    let answer = ''
    if (!argv.quiet) {
      answer = await ask(`Enter backup name (${defName}): `)
    }
    this.backup = `${this.sobr.dataDir}/dump/${_.isEmpty(answer) ? defName : answer}`
    if (!argv.quiet && fs.existsSync(`${this.backup}${argv.tgz ? '.tgz' : ''}`)) {
      let answer = ''
      while (!['y', 'n', 'yes', 'no'].includes(answer)) {
        answer = await ask('Backup name exists, overwrite? (y/n) ? ')
        answer = answer.toLowerCase()
      }
      if (['n', 'no'].includes(answer)) return logger.info('Model export canceled')
    }
    await rmfr(this.backup)
    await mkdir(this.backup)
    await this.dump(actions)
  }

  async dump (actions) {
    logger.info('Exporting:')
    for (let i = 0; i < actions.length; i++) {
      let schema
      try {
        schema = require(actions[i].file)({ sob: actions[i].sob, dbSql: 'dump' })
      } catch (err) {
        console.log(err)
        process.exit()
      }
      if (!(schema && schema.mixins.includes('dump'))) {
        logger.info(`- ${actions[i].name}\tNo model found or is an unknown model type, skipped`)
        continue
      }
      let db = this.dbs[actions[i].sob.name]
      if (!db) db = this.dbs.default
      const options = mergeClone({
        sequelize: db,
        modelName: schema.model.name || actions[i].name
      }, schema.model.options || {})
      const model = db.define(options.modelName, schema.model.define, schema.model.options)
      try {
        sequelizeStream(model.sequelize)
        const count = await persistModel(model, `${this.backup}/${model.name}.json`, { format: argv.jsonLine ? 'jsonl' : undefined })
        logger.info(`- ${actions[i].name}\t${count} record(s) written`)
      } catch (err) {
        logger.error(`- ${actions[i].name}\t${err.message}`)
      }
    }
    if (argv.tgz) {
      const basename = path.basename(this.backup)
      await compressing.tgz.compressDir(this.backup, `${this.sobr.dataDir}/dump/${basename}.tgz`)
      await rmfr(this.backup)
    }
    const file = path.resolve(`${this.backup}${argv.tgz ? '.tgz' : ''}`)
    logger.info(`Backup saved as '${file}'`)
  }
}

module.exports = Dumper
