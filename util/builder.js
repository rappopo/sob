const argv = require('minimist')(process.argv.slice(2))
const { _, age, mergeClone, resolveFixture, matcherFromCli, loadModel, DateTime } = require('../lib/helper')
const Sequelize = require('sequelize')
const Model = Sequelize.Model
const fs = require('fs')
const loggerName = '[SobBuilder]'
const logger = require('../lib/helper').logger({ name: loggerName })
const interactive = require('./interactive')
const ask = async q => {
  return interactive.ask(q, { loggerName })
}

class Table extends Model {}

class Builder {
  constructor ({ sobr, dbs, findSob }) {
    this.sobr = sobr
    this.dbs = dbs
    this.findSob = findSob
  }

  async run () {
    const actions = await matcherFromCli('build', this.sobr, this.findSob)
    if (!actions) return
    logger.info(`The following ${actions.length} model(s) will be built ${argv.noFixture ? 'without' : 'with'} fixtures`)
    _.each(actions, a => {
      logger.info(`- ${a.name}`)
    })
    let answer = ''
    while (!['y', 'n', 'yes', 'no'].includes(answer)) {
      answer = await ask('Are you sure to continue? This process can\'t be canceled nor undid: y/n ? ')
      answer = answer.toLowerCase()
    }
    if (['n', 'no'].includes(answer)) return logger.info('Model building canceled')
    await this.build(actions)
  }

  async build (actions) {
    for (let i = 0; i < actions.length; i++) {
      const start = DateTime.local()
      let schema
      try {
        schema = require(actions[i].file)({ sobr: this.sobr, sob: actions[i].sob, dbSql: 'build' })
      } catch (err) {
        console.log(err)
        process.exit()
      }
      if (schema && schema.mixins && schema.mixins.includes('sd')) {
        logger.warn(`${actions[i].name}\tSimple dataset model, skipped`)
        continue
      }
      if (!(schema && schema.mixins && schema.mixins.includes('build'))) {
        logger.warn(`${actions[i].name}\tNo model found or is an unknown model type, skipped`)
        continue
      }
      let db = this.dbs[actions[i].sob.name]
      let dbProtected = _.get(this.sobr, `sob.core.config.db.${actions[i].sob.name}.protected`, false)
      if (!db) {
        db = this.dbs.default
        dbProtected = _.get(this.sobr, 'sob.core.config.db.default.protected', false)
      }
      const options = mergeClone({
        sequelize: db,
        modelName: schema.model.name || actions[i].name
      }, schema.model.options || {})
      Table.init(schema.model.define, options)
      const executor = async data => {
        await Table.bulkCreate(data)
      }
      let fixtures = []
      try {
        if (dbProtected) throw new Error(`Database is protected`)
        await Table.sync({ force: true })
        const jsonl = `${this.sobr.dataDir}/fixture/${actions[i].name}.jsonl`
        if (!argv.noFixture) {
          if (fs.existsSync(jsonl)) {
            // TODO: merge with builtin one first!
            fixtures = await loadModel(Table, jsonl, { format: 'jsonl' })
          } else {
            fixtures = await resolveFixture(actions[i].sob, this.sobr, executor)
          }
        }
        logger.info(`${actions[i].name}\tSchema: OK, Fixtures: ${argv.noFixture ? 'Disabled' : ((_.isArray(fixtures) ? fixtures.length : fixtures) + ' record(s)')}, Duration: ${age(start)}`)
      } catch (err) {
        logger.error(`${actions[i].name}\t${err.message}`)
      }
    }
  }
}

module.exports = Builder
