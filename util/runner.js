global.Promise = require('bluebird')
Promise.config({ cancellation: true })

const os = require('os')
const { ServiceBroker } = require('moleculer')
const lockfile = require('proper-lockfile')
const SingleService = require('../mw/single-service')
const argv = require('minimist')(process.argv.slice(2))
const path = require('path')
const fs = require('fs')
const Sd = require('../factory/sd')
const Sequelize = require('sequelize')
const fg = require('fast-glob')
const isInstalled = require('is-installed')
const { _, toUnixPath, mergeClone, readConfig, getPkgPath, DateTime, age, parseDuration } = require('../lib/helper')
const logger = require('../lib/helper').logger({ name: '[SobRunner]' })
const cluster = require('cluster')
const sequelizeStream = require('../lib/sequelize-stream')
const DbSql = require('../factory/db-sql')

const stopSignals = [
  'SIGHUP', 'SIGINT', 'SIGQUIT', 'SIGILL', 'SIGTRAP', 'SIGABRT',
  'SIGBUS', 'SIGFPE', 'SIGUSR1', 'SIGSEGV', 'SIGUSR2', 'SIGTERM'
]

class Runner {
  constructor () {
    this.broker = null
    this.worker = null
    this.dbs = {}
    this.sobr = {} // Sob Root
  }

  async initSobRoot () {
    let dataDir = argv.dataDir || argv.d || process.cwd()
    dataDir = toUnixPath(path.resolve(dataDir))
    if (!fs.existsSync(dataDir)) {
      logger.error(`Dir not found: '${dataDir}'`)
      process.exit()
    }
    if (dataDir === toUnixPath(path.resolve(process.cwd()))) {
      dataDir += '/data'
      if (!fs.existsSync(dataDir)) fs.mkdirSync(dataDir)
    }
    _.each(['lock', 'db', 'fixture', 'sd', 'override', 'dump', 'tmp', 'persistence'], d => {
      const dir = `${dataDir}/${d}`
      if (!fs.existsSync(dir)) fs.mkdirSync(dir)
    })
    const appCfg = await readConfig(`${dataDir}/app.json`)
    this.sobr = mergeClone(require('../data/app.json'), appCfg)
    this.sobr.init.modules.push('app')
    this.sobr.dataDir = dataDir
    this.sobr.sob = {}
    let mods = ['rappopo-sob']
    _.each(['libs.before', 'libs.after', 'modules'], item => {
      _.each(_.get(this.sobr, `init.${item}`, []), i => {
        if (i.substr(0, 1) === '@') {
          const parts = i.split('/')
          mods.push(`${parts[0]}/${parts[1]}`)
        } else {
          mods.push(i.split('/')[0])
        }
      })
    })
    mods.push('app')
    mods = _.uniq(mods)
    for (let i = 0; i < mods.length; i++) {
      await this.buildSob(mods[i])
    }
  }

  async buildSob (p) {
    if (p === 'app') {
      const dir = toUnixPath(process.cwd())
      const cfg = await readConfig(`${dir}/data/config.json`)
      this.sobr.sob.app = {
        name: 'app',
        pkg: 'app',
        pkgDir: dir,
        config: cfg
      }
      return
    }
    const installed = isInstalled.sync(p)
    if (!installed) logger.error(new Error(`Can't find '${p}' anywhere. Is it installed?`), true)
    const dir = getPkgPath(p)
    const pkg = require(`${dir}/package.json`)
    if (!_.has(pkg, 'sob')) logger.error(new Error(`Invalid Rappopo Sob package '${p}': doesn't have 'sob' key`), true)
    let cfg = await readConfig(`${dir}/data/config.json`)
    const override = await readConfig(`${this.sobr.dataDir}/override/config/${pkg.sob.name}.json`)
    cfg = _.merge(cfg, override)
    this.sobr.sob[pkg.sob.name] = {
      name: pkg.sob.name,
      pkg: p,
      pkgDir: dir,
      config: cfg
    }
  }

  findSob (name, sobr = {}) {
    if (name === 'app') return _.cloneDeep((this ? this.sobr : sobr).sob.app)
    let pkg = {}
    _.forOwn((this ? this.sobr : sobr).sob, (v, k) => {
      if (v.pkg === name) {
        pkg = v
        return false
      }
    })
    return _.cloneDeep(pkg)
  }

  async initModules () {
    if (this.sobr.init.modules.length === 0) return
    const start = DateTime.local()
    this.broker.logger.info(`Loading ${this.sobr.init.modules.length} modules...`)
    let count = 0
    for (let i = 0; i < this.sobr.init.modules.length; i++) {
      const m = this.sobr.init.modules[i]
      const cm = await this.initModule(m)
      const cs = await this.initSd(m)
      count += (cm + cs)
    }
    this.broker.logger.info(`Initialize ${count} services in ${age(start)}`)
  }

  async initLibs (key) {
    const arr = this.sobr.init.libs[key]
    if (arr.length === 0) return
    for (let i = 0; i < arr.length; i++) {
      const m = arr[i]
      await this.initLib(m)
    }
  }

  async createService (def, file) {
    const lockfilePath = `${this.sobr.dataDir}/lock/${def.name}.lock`
    if (!(this.workerCount > 1 && def.settings.singleService)) return this.broker.createService(def)
    try {
      const fn = await lockfile.lock(file, { lockfilePath })
      const svc = this.broker.createService(def)
      svc.unlockFn = fn
    } catch (err) {
      // this.broker.logger.warn(`'${def.name}' Unable to start as single service. Another node may acquired the lock already`)
    }
  }

  async initModule (name) {
    let dir
    if (_.isString(name)) {
      dir = name === 'app' ? this.sobr.sob.app.pkgDir : getPkgPath(name)
    } else {
      dir = name.pkgDir
      name = name.pkg
    }
    let count = 0
    const files = await fg(dir + '/svc/**/*.js')
    if (files.length > 0) {
      for (let i = 0; i < files.length; i++) {
        const start = DateTime.local()
        const sob = this.findSob(name)
        const basename = files[i].replace(dir + '/svc/', '').replace('.js', '')
        sob.basename = basename
        let service = require(`${files[i]}`)

        let db = this.dbs[sob.name]
        if (!db) db = this.dbs.default
        const dbSql = DbSql({ sob, broker: this.broker, db })
        const sd = Sd({ sob, broker: this.broker, sd: this.sobr.sob.core.sd })
        if (_.isFunction(service)) service = service({ sob, broker: this.broker, dbSql, sd, sobr: this.sobr })

        service.settings = service.settings || {}
        if (service.settings.entityValidator) service.settings.validatorSchema = _.cloneDeep(service.settings.entityValidator)
        service.name = _.camelCase(sob.name + ' ' + basename)
        service.settings.sob = _.omit(sob, ['config'])
        service.settings.$dependencyTimeout = parseDuration(this.sobr.service.waitTimeout)
        if (service.model) service.model.name = service.model.name || service.name
        await this.createService(service, files[i])
        this.broker.logger.debug(`Loading '${service.name}' in ${age(start)}`)
        count++
      }
    }
    return count
  }

  async initLib (fname) {
    const parts = fname.split('/')
    let name = parts[0]
    if (fname.substr(0, 1) === '@') {
      name = `${parts[0]}/${parts[1]}`
      parts.shift()
      parts.shift()
    } else {
      parts.shift()
    }
    const sob = this.findSob(name)
    const dir = sob.pkgDir
    let file = `${dir}/${parts.join('/')}`
    if (file.substr(file.length - 3) !== '.js') file += '.js'
    const basename = file.replace(dir + '/lib/', '').replace(dir, '').replace('.js', '')
    let service = require(`${file}`)
    if (_.isFunction(service)) service = service({ sob, broker: this.broker, sobr: this.sobr })
    service.settings = service.settings || {}
    service.name = _.camelCase(sob.name + ' ' + basename)
    sob.basename = basename
    sob.service = service.name
    service.settings.sob = _.omit(sob, ['config'])
    await this.createService(service, file)
  }

  async initSd (name) {
    let dir
    let count = 0
    if (_.isString(name)) {
      dir = name === 'app' ? this.sobr.sob.app.pkgDir : getPkgPath(name)
    } else {
      dir = name.pkgDir
      name = name.pkg
    }
    const files = await fg(dir + '/data/sd/**/*.{json,yaml,yml}')
    if (files.length > 0) {
      const sob = this.findSob(name)
      for (let i = 0; i < files.length; i++) {
        const start = DateTime.local()
        const basename = files[i].replace(dir + '/data/sd/', '').replace('.json', '').replace('.yml', '').replace('.yaml', '')
        sob.basename = basename
        let definition = await readConfig(`${files[i]}`)
        if (_.isArray(definition)) {
          if (definition.length === 0) continue
          const schema = {}
          _.forOwn(definition[0], (v, k) => {
            if (_.isNumber(v)) {
              schema[k] = { type: 'double' }
            } else if (_.isBoolean(v)) {
              schema[k] = { type: 'boolean' }
            } else {
              schema[k] = { type: 'string' }
            }
            if (k === 'id') schema[k].primaryKey = true
          })
          definition = { schema, fixture: definition }
        }
        _.forOwn(definition.schema, (v, k) => {
          v.type = v.type.toUpperCase()
          if (v.type === 'STRING' && _.has(v, 'size')) {
            v.type = Sequelize[v.type](v.size)
          } else {
            v.type = Sequelize[v.type]
          }
          definition.schema[k] = v
        })
        // final fixtures
        const item = readConfig(`${this.sobr.dataDir}/override/sd/${sob.name}/${sob.basename}.json`)
        if (!_.isEmpty(item) && _.isArray(item)) definition.fixture = _.merge(definition.fixture, item)
        // final schema
        const svcName = _.camelCase(sob.name + ' sd ' + basename)
        const schema = {
          name: svcName,
          mixins: [Sd({ sob: sob.config, broker: this.broker, sd: this.sobr.sob.core.sd })],
          model: {
            name: svcName,
            define: definition.schema
          },
          settings: {
            sob,
            sd: true,
            fields: _.keys(definition.schema)
          },
          async afterConnected () {
            const fixture = definition.fixture || []
            if (_.isEmpty(fixture)) return
            await this.adapter.model.bulkCreate(fixture)
            sequelizeStream(this.adapter.model.sequelize)
            if (_.isFunction(this.schema.afterStreamAttached)) this.schema.afterStreamAttached.call(this)
          }
        }
        await this.createService(schema, files[i])
        this.broker.logger.debug(`Loading '${svcName}' in ${age(start)}`)
        count++
      }
    }
    return count
  }

  async initRunner () {
    let runners = []
    for (let i = 0; i < this.broker.services.length; i++) {
      const svc = this.broker.services[i]
      if (svc.schema.run) {
        if (_.isFunction(svc.schema.run)) runners.push({ priority: 50, svc, handler: svc.schema.run })
        else runners.push({ priority: svc.schema.run.priority || 50, svc, handler: svc.schema.run.handler })
      }
    }
    runners = _.orderBy(runners, ['priority'], ['asc'])
    for (let i = 0; i < runners.length; i++) {
      await runners[i].handler.apply(runners[i].svc)
    }
  }

  initCoreDB () {
    if (!this.sobr.sob.core) return
    const Op = Sequelize.Op
    const operatorsAliases = {
      $eq: Op.eq,
      $ne: Op.ne,
      $gte: Op.gte,
      $gt: Op.gt,
      $lte: Op.lte,
      $lt: Op.lt,
      $not: Op.not,
      $in: Op.in,
      $notIn: Op.notIn,
      $is: Op.is,
      $like: Op.like,
      $notLike: Op.notLike,
      $iLike: Op.iLike,
      $notILike: Op.notILike,
      $regexp: Op.regexp,
      $notRegexp: Op.notRegexp,
      $iRegexp: Op.iRegexp,
      $notIRegexp: Op.notIRegexp,
      $between: Op.between,
      $notBetween: Op.notBetween,
      $overlap: Op.overlap,
      $contains: Op.contains,
      $contained: Op.contained,
      $adjacent: Op.adjacent,
      $strictLeft: Op.strictLeft,
      $strictRight: Op.strictRight,
      $noExtendRight: Op.noExtendRight,
      $noExtendLeft: Op.noExtendLeft,
      $and: Op.and,
      $or: Op.or,
      $any: Op.any,
      $all: Op.all,
      $values: Op.values,
      $col: Op.col
    }

    _.forOwn(this.sobr.sob.core.config.db, (v, k) => {
      if (!this.dbs[k]) {
        let mem = false
        if (v.dialect === 'sqlite') {
          if (v.storage !== ':memory:') {
            if (path.isAbsolute(v.storage)) {
              if (!fs.existsSync(path.dirname(v.storage))) {
                logger.error(`Invalid DB path '${v.storage}'`)
                process.exit()
              }
            } else {
              const basename = path.basename(v.storage, '.sqlite3')
              const dir = `${this.sobr.dataDir}/db`
              v.storage = `${dir}/${basename}.sqlite3`
              if (!fs.existsSync(dir)) fs.mkdirSync(dir)
            }
          } else {
            mem = true
          }
        }
        v.operatorsAliases = operatorsAliases
        this.dbs[k] = new Sequelize(v)
        this.dbs[k].noSync = !mem
      }
    })
    this.sobr.sob.core.db = this.dbs
    this.sobr.sob.core.config.sd.operatorsAliases = operatorsAliases
    this.sobr.sob.core.sd = new Sequelize('sqlite://:memory:?cache=shared', this.sobr.sob.core.config.sd)
  }

  startBroker () {
    this.worker = cluster.worker
    const options = mergeClone(this.sobr.broker, {
      middlewares: [SingleService]
    })
    options.nodeID = options.nodeID || `${os.hostname().toLowerCase()}-${process.pid}-${this.worker.id}`
    if (process.env.NODE_ENV === 'production') options.logLevel = 'warn'
    if (argv.debug) argv.logLevel = 'debug'
    if (argv.logLevel && 'trace, debug, info, warn, error, fatal'.split(', ').includes(argv.logLevel)) options.logLevel = argv.logLevel
    if (argv.nocache) options.cacher = false
    this.broker = new ServiceBroker(options)
    this.broker.sobr = this.sobr
    return new Promise((resolve, reject) => {
      Promise.resolve()
        .then(() => this.broker.start())
        .then(() => this.initCoreDB())
        .then(() => this.initLibs('before'))
        .then(() => this.initModules())
        .then(() => this.initLibs('after'))
        .then(() => this.initRunner())
        .then(() => {
          this.broker.broadcast('sys:ready')
          if (this.sobr.init.repl && this.workerCount === 1) this.broker.repl()
          resolve()
        })
        .catch(reject)
    })
  }

  async buildModels () {
    this.initCoreDB()
    const Builder = require('./builder')
    const builder = new Builder({ sobr: this.sobr, dbs: this.dbs, findSob: this.findSob })
    await builder.run()
    process.exit()
  }

  async dumpModels () {
    this.initCoreDB()
    const Dumper = require('./dumper')
    const dumper = new Dumper({ sobr: this.sobr, dbs: this.dbs, findSob: this.findSob })
    await dumper.run()
    process.exit()
  }

  async startWorkers () {
    let stopping = false

    cluster.on('exit', function (worker, code) {
      if (!stopping) {
        if (process.env.NODE_ENV === 'production' && code !== 0) {
          logger.info(`The worker #${worker.id} has disconnected`)
          logger.info(`Worker #${worker.id} restarting...`)
          cluster.fork()
          logger.info(`Worker #${worker.id} restarted`)
        } else {
          process.exit(code)
        }
      }
    })

    logger.info(`Starting ${this.workerCount} workers...`)

    for (let i = 0; i < this.workerCount; i++) {
      cluster.fork()
    }

    stopSignals.forEach(function (signal) {
      process.on(signal, () => {
        logger.info(`Got ${signal}, stopping workers...`)
        stopping = true
        cluster.disconnect(function () {
          logger.info('All workers stopped, exiting.')
          process.exit(0)
        })
      })
    })
  }

  async start () {
    if (argv.prod) process.env.NODE_ENV = 'production'
    await this.initSobRoot()
    if (argv.build) return this.buildModels()
    if (argv.dump) return this.dumpModels()
    this.workerCount = _.isInteger(this.sobr.init.worker) && this.sobr.init.worker > 0 ? this.sobr.init.worker : os.cpus().length
    if (cluster.isMaster) return this.startWorkers()
    else return this.startBroker()
  }
}

module.exports = Runner
