#!/usr/bin/env node
const argv = require('minimist')(process.argv.slice(2))
if (argv.create) {
  require('./creator')()
    .then(process.exit)
    .catch(console.log)
} else {
  const Runner = require('./runner')
  const runner = new Runner()
  runner.start()
}
