const kleur = require('kleur')

const readline = require('readline-promise').default
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
})

const ask = async (q, options = {}) => {
  return rl.questionAsync(kleur.grey(options.loggerName || '[Sob]') + ' ' + kleur[options.color || 'white']().bold(q) + '')
}

module.exports = { ask }
