const { DateTime, persistModel, loadModel, age, _ } = require('../lib/helper')
const compressing = require('compressing')
const argv = require('minimist')(process.argv.slice(2))

module.exports = () => {
  return {
    afterStreamAttached () {
      setTimeout(() => {
        this.load()
          .then(() => {
            if (_.isFunction(this.schema.afterLoad)) this.schema.afterLoad.call(this)
          })
        this.intvAutoPersist = setInterval(this.persist, 30 * 1000)
      }, 5000)
    },
    stopped () {
      if (this.intvAutoPersist) clearInterval(this.intvAutoPersist)
    },
    methods: {
      load () {
        if (argv.skipLoad) {
          if (_.isBoolean(argv.skipLoad)) {
            this.logger.warn('All persistence loading disabled')
            return Promise.resolve()
          }
          const skipLoad = _.isString(argv.skipLoad) ? argv.skipLoad.split(',') : argv.skipLoad
          if (skipLoad.includes(this.name)) {
            this.logger.warn(`Persistence loading disabled for this model`)
            return Promise.resolve()
          }
        }
        const start = DateTime.local()
        return new Promise((resolve, reject) => {
          const file = `${this.broker.sobr.dataDir}/persistence/${this.name}.jsonl`
          loadModel(this.model, file, { format: 'jsonl' })
            .then(result => {
              this.logger.debug(`Loading ${result} record(s), duration: ${age(start)}`)
              resolve()
            })
            .catch(reject)
        })
      },
      persist () {
        if (argv.skipPersist) {
          if (_.isBoolean(argv.skipPersist)) {
            this.logger.warn('All persistence saving disabled')
            return Promise.resolve()
          }
          const skipPersist = _.isString(argv.skipPersist) ? argv.skipPersist.split(',') : argv.skipPersist
          if (skipPersist.includes(this.name)) {
            this.logger.warn(`Persistence saving disabled for this model`)
            return Promise.resolve()
          }
        }
        const start = DateTime.local()
        return new Promise((resolve, reject) => {
          const file = `${this.broker.sobr.dataDir}/persistence/${this.name}.jsonl`
          persistModel(this.model, file, { format: 'jsonl' })
            .then(result => {
              this.logger.debug(`Persisting ${result} record(s), duration: ${age(start)}`)
              return compressing.gzip.compressFile(file, `${file}.bck.gz`)
            })
            .then(resolve)
            .catch(reject)
        })
      }
    }
  }
}
