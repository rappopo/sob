const { _, fetch, mergeClone, parseDuration } = require('../lib/helper')

module.exports = ({ sobr }) => {
  const config = _.get(sobr, 'sob.core.config.helper.fetchUrl', {})
  if (_.has(config, 'timeout')) config.timeout = parseDuration(config.timeout)
  return {
    methods: {
      async fetchUrl (url, options) {
        const opts = mergeClone(config, options)
        return fetch(url, opts)
      }
    }
  }
}
