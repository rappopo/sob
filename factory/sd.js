const DbSqlAdapter = require('../lib/db-sql-adapter')
const SqlCommon = require('./sql-common')
const { _ } = require('../lib/helper')
const sequelizeStream = require('../lib/sequelize-stream')

module.exports = ({ sob, broker, sd }) => {
  if (sob === 'build') return 'sd'
  return {
    mixins: [SqlCommon({ sob, broker })],
    adapter: new DbSqlAdapter(sd),
    settings: {
      isSd: true
    },
    afterConnected () {
      sequelizeStream(this.adapter.model.sequelize)
      if (_.isFunction(this.schema.afterStreamAttached)) this.schema.afterStreamAttached.call(this)
    }
  }
}
