const DbSqlAdapter = require('../lib/db-sql-adapter')
const SqlCommon = require('./sql-common')
const { _, loadModel, resolveFixture } = require('../lib/helper')
const fs = require('fs')
const sequelizeStream = require('../lib/sequelize-stream')

module.exports = ({ sob, broker, db }) => {
  if (sob === 'build') return 'dbSql'
  return {
    mixins: [SqlCommon({ sob, broker })],
    adapter: new DbSqlAdapter(db),
    afterConnected () {
      const db = this.adapter.model.sequelize
      sequelizeStream(db)
      if (db.options.dialect === 'sqlite' && db.options.storage === ':memory:') {
        const jsonl = `${this.broker.sobr.dataDir}/fixture/${this.name}.jsonl`
        const executor = async data => {
          await this.adapter.model.bulkCreate(data)
        }
        if (fs.existsSync(jsonl)) {
          loadModel(this.adapter.model, jsonl, { format: 'jsonl' })
        } else {
          resolveFixture(sob, this.broker.sobr, executor)
        }
      }
      if (_.isFunction(this.schema.afterStreamAttached)) this.schema.afterStreamAttached.call(this)
    }
  }
}
