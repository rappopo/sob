const DbService = require('moleculer-db')
const { _, createError, isSet } = require('../lib/helper')
const { flatten } = require('flat')
const { EntityNotFoundError } = require('moleculer-db/src/errors')
const { MoleculerClientError } = require('moleculer').Errors
const context = require('../lib/context')
require('sequelize/lib/utils/deprecations').noStringOperators = () => {}

module.exports = ({ broker }) => {
  const coreConfig = broker.sobr.sob.core.config
  return {
    mixins: [DbService],
    settings: {
      pageSize: _.get(coreConfig, 'web.api.pagination.default', 10),
      maxPageSize: _.get(coreConfig, 'web.api.pagination.max', 50)
    },
    entityCreated (json, ctx) {
      if (this.broker.cacher) this.broker.cacher.clean(`${this.name}.**`)
      this.broadcastRecordHandler('created', { value: json, params: ctx.params })
    },
    entityUpdated (json, ctx) {
      if (this.broker.cacher) this.broker.cacher.clean(`${this.name}.**`)
      this.broadcastRecordHandler('updated', { value: json.new, oldValue: json.old, params: ctx.params })
    },
    entityRemoved (json, ctx) {
      if (this.broker.cacher) this.broker.cacher.clean(`${this.name}.**`)
      this.broadcastRecordHandler('removed', { value: json, params: ctx.params })
    },
    actions: {
      modelFindAll (ctx) {
        return this.adapter.model.findAll(ctx.params || {})
      },
      modelTruncate (ctx) {
        return this.adapter.model.destroy({ truncate: true })
      },
      modelDestroy (ctx) {
        return this.adapter.model.destroy(ctx.params || {})
      },
      modelBulkCreate (ctx) {
        return this._bulkCreate(ctx)
      },
      modelUpsert (ctx) {
        return this.adapter.model.upsert(ctx.params)
      },
      upsert (ctx) {
        const params = ctx.params
        return new Promise((resolve, reject) => {
          if (!params.id) throw createError('ID is missing')
          return this.adapter.model.findOne({
            where: { id: ctx.params.id }
          })
            .then(result => {
              if (!result) return this._create(ctx, params)
              ctx.params = _.omit(params, ['createdAt', 'updatedAt'])
              return this._update(ctx, ctx.params)
            })
            .then(result => {
              resolve(result)
            })
            .catch(err => {
              reject(err)
            })
        })
      },
      find: {
        params: {
          sort: [
            { type: 'string', optional: true },
            { type: 'array', optional: true },
            { type: 'object', optional: true }
          ]
        }
      },
      apiList: {
        cache: {
          enabled: context.enableCache,
          keys: ['populate', 'fields', 'page', 'pageSize', 'sort', 'search', 'searchFields', 'query']
        },
        rest: 'GET /',
        params: {
          populate: [
            { type: 'string', optional: true },
            { type: 'array', optional: true, items: 'string' }
          ],
          fields: [
            { type: 'string', optional: true },
            { type: 'array', optional: true, items: 'string' }
          ],
          page: { type: 'number', integer: true, min: 1, optional: true, convert: true },
          pageSize: { type: 'number', integer: true, min: 0, optional: true, convert: true },
          sort: [
            { type: 'string', optional: true },
            { type: 'array', optional: true },
            { type: 'object', optional: true }
          ],
          search: { type: 'string', optional: true },
          searchFields: [
            { type: 'string', optional: true },
            { type: 'array', optional: true, items: 'string' }
          ],
          query: [
            { type: 'object', optional: true },
            { type: 'string', optional: true }
          ],
          rsql: { type: 'string', optional: true },
          nql: { type: 'string', optional: true },
          format: { type: 'string', optional: true }
        },
        handler (ctx) {
          return this._apiList(ctx, this.sanitizeParams(ctx, ctx.params))
        }
      },
      apiGet: {
        cache: {
          enabled: context.enableCache,
          keys: ['id', 'populate', 'fields', 'mapping']
        },
        rest: 'GET /:id',
        params: {
          id: [
            { type: 'string' },
            { type: 'number' },
            { type: 'array' }
          ],
          populate: [
            { type: 'string', optional: true },
            { type: 'array', optional: true, items: 'string' }
          ],
          fields: [
            { type: 'string', optional: true },
            { type: 'array', optional: true, items: 'string' }
          ],
          mapping: { type: 'boolean', optional: true }
        },
        handler (ctx) {
          return this._apiGet(ctx, this.sanitizeParams(ctx, ctx.params))
        }
      },
      apiCreate: {
        rest: 'POST /',
        handler (ctx) {
          return this._apiCreate(ctx, ctx.params)
        }
      },
      apiUpdate: {
        rest: 'PUT /:id',
        handler (ctx) {
          return this._apiUpdate(ctx, ctx.params)
        }
      },
      apiRemove: {
        rest: 'DELETE /:id',
        params: {
          id: { type: 'any' }
        },
        handler (ctx) {
          return this._apiRemove(ctx, this.sanitizeParams(ctx, ctx.params))
        }
      }
    },
    methods: {
      isUserAware (ctx) {
        return context.isUserAware(ctx, this.schema)
      },
      isSiteAware (ctx) {
        return context.isSiteAware(ctx, this.schema)
      },
      isShareable (ctx) {
        return context.isShareable(ctx, this.schema)
      },
      isAdmin (ctx) {
        return context.isAdmin(ctx)
      },
      isManager (ctx) {
        return context.isManager(ctx)
      },
      isMine (ctx, rec) {
        return this.isUserAware(ctx) && rec.userId === ctx.meta.user.id
      },
      broadcastRecordHandler (type, json) {
        const sobName = _.snakeCase(this.name).split('_')[0]
        let bc = this.settings.broadcastRecord
        const cbc = _.get(this.broker, `sobr.sob.${sobName}.config.broadcastRecord.${this.name}`)
        if (isSet(cbc)) bc = cbc
        if (bc) this.broker.broadcast(this.name + _.upperFirst(type), json, ['record'])
      },
      getDistinctRowFromQuery (ctx) {
        return new Promise((resolve, reject) => {
          context.buildApiFilter(ctx, ctx.action.service.name)
            .then(query => {
              const params = { query, limit: 1 }
              return this.adapter.find(params)
            })
            .then(result => {
              if (result.length === 0) throw new EntityNotFoundError(_.get(ctx.params, 'query.id'))
              resolve(result[0])
            })
            .catch(reject)
        })
      },
      _bulkCreate (ctx, params) {
        const options = _.merge({
          hooks: false,
          ignoreDuplicates: false,
          validate: false,
          updateOnDuplicate: Object.keys(this.adapter.model.rawAttributes)
        }, (ctx.params || {}).options || {})
        const entities = _.isArray(ctx.params) ? ctx.params : (ctx.params || {}).entities
        return this.adapter.model.bulkCreate(entities, options)
      },
      _apiList (ctx, params = {}) {
        return new Promise((resolve, reject) => {
          let endResult
          context.buildApiFilter(ctx, ctx.action.service.name)
            .then(query => {
              params.query = query
              const countParams = Object.assign({}, params)
              // Remove pagination params
              if (countParams && countParams.limit) countParams.limit = null
              if (countParams && countParams.offset) countParams.offset = null
              if (params.limit == null) {
                if (this.settings.limit > 0 && params.pageSize > this.settings.limit) params.limit = this.settings.limit
                else params.limit = params.pageSize
              }
              if (!params.fields) params.fields = this.settings.defFields
              return Promise.all([
                this.adapter.find(params),
                this.adapter.count(countParams)
              ])
            })
            .then(res => {
              endResult = res
              return this.transformDocuments(ctx, params, res[0])
            })
            .then(docs => {
              resolve({
                rows: docs,
                total: endResult[1],
                page: params.page,
                pageSize: params.pageSize,
                totalPages: Math.floor((endResult[1] + params.pageSize - 1) / params.pageSize)
              })
            })
            .catch(reject)
        })
      },
      _apiCreate (ctx, params) {
        return new Promise((resolve, reject) => {
          const entity = params
          if (this.isSiteAware(ctx)) entity.siteId = ctx.meta.site.id
          if (this.isUserAware(ctx)) {
            if (this.isManager(ctx)) {
              if (!_.has(entity, 'userId')) entity.userId = ctx.meta.user.id
            } else entity.userId = ctx.meta.user.id
          }
          let endResult
          let existing
          this.validateEntity(entity)
            // Apply idField
            .then(entity => this.adapter.beforeSaveTransformID(entity, this.settings.idField))
            .then(entity => {
              existing = entity
              const hook = this[_.camelCase('before ' + ctx.action.rawName)]
              if (hook) return hook.call(this, ctx, { params, entity })
              return entity
            })
            .then(entity => {
              if (!entity) entity = existing
              return this.adapter.insert(entity)
            })
            .then(doc => {
              return this.transformDocuments(ctx, {}, doc)
            })
            .then(doc => {
              endResult = doc
              ctx.meta.doc = doc
              const hook = this[_.camelCase('after ' + ctx.action.rawName)]
              if (hook) return hook.call(this, ctx, { params, doc: endResult })
              return endResult
            })
            .then(json => {
              if (!json) json = endResult
              this.entityChanged('created', { doc: json, params }, ctx)
              resolve(json)
            })
            .catch(reject)
        })
      },
      _apiInsert (ctx, params) {
        return Promise.resolve()
          .then(() => {
            if (Array.isArray(params.entities)) {
              _.each(params.entities, e => {
                if (this.isSiteAware(ctx)) e.siteId = ctx.meta.site.id
                if (this.isUserAware(ctx)) {
                  if (this.isManager(ctx)) {
                    if (!_.has(e, 'userId')) e.userId = ctx.meta.user.id
                  } else e.userId = ctx.meta.user.id
                }
              })
              return this.validateEntity(params.entities)
                .then(entities => {
                  if (this.settings.idField === '_id') return entities
                  return entities.map(entity => this.adapter.beforeSaveTransformID(entity, this.settings.idField))
                })
                .then(entities => this.adapter.insertMany(entities, {
                  returning: true,
                  hooks: false,
                  ignoreDuplicates: false,
                  validate: false,
                  updateOnDuplicate: Object.keys(this.adapter.model.rawAttributes)
                }))
            } else if (params.entity) {
              if (this.isSiteAware(ctx)) params.entity.siteId = ctx.meta.site.id
              if (this.isUserAware(ctx)) {
                if (this.isManager(ctx)) {
                  if (!_.has(params.entity, 'userId')) params.entity.userId = ctx.meta.user.id
                } else params.entity.userId = ctx.meta.user.id
              }
              return this.validateEntity(params.entity)
                // Apply idField
                .then(entity => this.adapter.beforeSaveTransformID(entity, this.settings.idField))
                .then(entity => this.adapter.insert(entity))
            }
            return Promise.reject(new MoleculerClientError('Invalid request! The "params" must contain "entity" or "entities"!', 400))
          })
          .then(docs => this.transformDocuments(ctx, {}, docs))
          .then(json => this.entityChanged('created', json, ctx).then(() => json))
      },
      _apiGet (ctx, params) {
        ctx.params = { query: { id: params.id }, _su: params._su }
        return this.getDistinctRowFromQuery(ctx)
      },
      _apiUpdate (ctx, params) {
        if (this.isSiteAware(ctx)) {
          delete params.siteId
        }
        delete params.builtIn
        let id
        let sets = {}
        Object.keys(params).forEach(prop => {
          if (prop === 'id' || prop === this.settings.idField) id = this.decodeID(params[prop])
          else sets[prop] = params[prop]
        })
        if (this.settings.useDotNotation) sets = flatten(sets, { safe: true })
        let endResult
        let existing
        return new Promise((resolve, reject) => {
          ctx.params = { query: { id }, _su: ctx.params._su }
          this.getDistinctRowFromQuery(ctx)
            .then(doc => {
              ctx.meta.oldDoc = doc.get({ plain: true })
              existing = doc
              const hook = this[_.camelCase('before ' + ctx.action.rawName)]
              if (hook) return hook.call(this, ctx, { params, oldDoc: existing, doc })
              return doc
            })
            .then(doc => {
              if (!doc) doc = existing
              if (doc.builtIn) sets = _.omit(sets, ['status'])
              let allowShared = false
              if (this.isShareable(ctx)) {
                delete sets.ownerId
                const allowTeam = (ctx.meta.role.team || []).includes(doc.ownerId) && doc.shared === 'WT'
                const allowAll = doc.shared === 'WA'
                const allowSelf = doc.ownerId === ctx.meta.user.id
                if (allowTeam || allowAll) delete sets.shared
                allowShared = allowSelf || allowTeam || allowAll
              }
              if (params._sysForce || allowShared || this.isMine(ctx, doc) || this.isManager(ctx)) return this.adapter.updateById(id, { $set: sets })
              throw createError('Access Denied', 403)
            })
            .then(doc => {
              if (!doc) throw new EntityNotFoundError(id)
              return this.transformDocuments(ctx, {}, doc)
            })
            .then(doc => {
              endResult = doc
              ctx.meta.doc = endResult
              const hook = this[_.camelCase('after ' + ctx.action.rawName)]
              if (hook) return hook.call(this, ctx, { params, doc: endResult, oldDoc: existing })
              return endResult
            })
            .then(json => {
              if (!json) json = endResult
              this.entityChanged('updated', { oldDoc: existing, doc: json, params }, ctx)
              resolve(endResult)
            })
            .catch(reject)
        })
      },
      _apiRemove (ctx, params) {
        const id = this.decodeID(params.id)
        let endResult
        let existing
        return new Promise((resolve, reject) => {
          ctx.params = { query: { id }, _su: ctx.params._su }
          this.getDistinctRowFromQuery(ctx)
            .then(doc => {
              existing = doc
              ctx.meta.doc = doc.get({ plain: true })
              const hook = this[_.camelCase('before ' + ctx.action.rawName)]
              if (hook) return hook.call(this, ctx, { params, doc })
              return doc
            })
            .then(doc => {
              if (!doc) doc = existing
              if (doc.builtIn) throw new MoleculerClientError('Built-in record can\'t be removed', 400)
              if (this.isMine(ctx, doc) || this.isManager(ctx)) return this.adapter.removeById(id)
              throw createError('Access Denied', 403)
            })
            .then(doc => {
              if (!doc) throw new EntityNotFoundError(id)
              return this.transformDocuments(ctx, {}, doc)
            })
            .then(doc => {
              endResult = doc
              const hook = this[_.camelCase('after ' + ctx.action.rawName)]
              if (hook) return hook.call(this, ctx, { params, doc: endResult })
              return endResult
            })
            .then(json => {
              if (!json) json = endResult
              this.entityChanged('removed', { doc: json, params }, ctx)
              resolve(json)
            })
            .catch(reject)
        })
      },
      sanitizeParams (ctx, params) {
        const p = Object.assign({}, params)
        if (typeof (p.limit) === 'string') p.limit = Number(p.limit)
        if (typeof (p.offset) === 'string') p.offset = Number(p.offset)
        if (typeof (p.page) === 'string') p.page = Number(p.page)
        if (typeof (p.pageSize) === 'string') p.pageSize = Number(p.pageSize)
        // Convert from string to POJO
        if (typeof (p.query) === 'string') p.query = JSON.parse(p.query)
        if (typeof (p.sort) === 'string') p.sort = p.sort.replace(/,/g, ' ').split(' ')
        if (typeof (p.fields) === 'string') p.fields = p.fields.replace(/,/g, ' ').split(' ')
        if (typeof (p.populate) === 'string') p.populate = p.populate.replace(/,/g, ' ').split(' ')
        if (typeof (p.searchFields) === 'string') p.searchFields = p.searchFields.replace(/,/g, ' ').split(' ')
        if (ctx.action.name.endsWith('list') || ctx.action.name.endsWith('List')) {
          // Default `pageSize`
          if (!p.pageSize) p.pageSize = this.settings.pageSize
          // Default `page`
          if (!p.page) p.page = 1
          // Limit the `pageSize`
          if (this.settings.maxPageSize > 0 && p.pageSize > this.settings.maxPageSize) p.pageSize = this.settings.maxPageSize
          // Calculate the limit & offset from page & pageSize
          p.limit = p.pageSize
          p.offset = (p.page - 1) * p.pageSize
        }
        // Limit the `limit`
        if (this.settings.maxLimit > 0 && p.limit > this.settings.maxLimit) p.limit = this.settings.maxLimit
        return p
      }
    }
  }
}
