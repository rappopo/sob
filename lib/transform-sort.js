const _ = require('lodash')

module.exports = function (paramSort) {
  let sort = paramSort
  if (_.isString(sort)) sort = sort.replace(/,/, ' ').split(' ')

  if (Array.isArray(sort)) {
    const sortObj = []
    sort.forEach(s => {
      if (s.startsWith('-')) sortObj.push([s.slice(1), 'DESC'])
      else sortObj.push([s, 'ASC'])
    })
    return sortObj
  }

  if (_.isObject(sort)) {
    return Object.keys(sort).map(name => [name, sort[name] > 0 ? 'ASC' : 'DESC'])
  }
  return []
}
