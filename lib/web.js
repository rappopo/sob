const ApiService = require('moleculer-web')
const _ = require('lodash')

const authorize = require('./web/authorize')
const reformatError = require('./web/reformat-error')

module.exports = ({ sob, broker }) => {
  const config = sob.config
  config.web.api = config.web.api || {}
  config.web.root = config.web.root || {}

  let routes = []
  _.each(['root', 'upload', 'api', 'public'], item => {
    const fn = require('./web/build-' + item)
    const items = fn(config, broker)
    if (items.length > 0) routes = _.concat(routes, items)
  })

  return {
    name: 'web',
    mixins: [ApiService],
    settings: {
      port: config.web.port || 1313,
      ip: config.web.ip || '0.0.0.0',
      etag: config.web.etag || false,
      rateLimit: config.web.rateLimit,
      cors: config.web.cors,
      routes
    },
    actions: {
      getServer (ctx) {
        return this.server
      }
    },
    methods: {
      reformatError,
      authorize
    }
  }
}
