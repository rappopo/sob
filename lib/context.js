const _ = require('lodash')
const fs = require('fs-extra')
const { splitString, createError, generateId } = require('./helper')
const path = require('path')
const rsql = require('rsql-mongodb')
const mime = require('mime-types')
const nql = require('@nexes/nql')

const isUserAware = (ctx, schema) => {
  if (ctx.meta.tenancy) return false
  if (!_.get(schema, 'model.define.userId')) return false
  if (!ctx.meta.user) return false
  return true
}

const isSiteAware = (ctx, schema) => {
  if (ctx.meta.tenancy) return false
  if (!_.get(schema, 'model.define.siteId')) return false
  if (!ctx.meta.site) return false
  return true
}

const isShareable = (ctx, schema) => {
  if (ctx.meta.tenancy) return false
  if (!_.get(schema, 'model.define.ownerId')) return false
  if (!_.get(schema, 'model.define.shared')) return false
  return true
}

const isAdmin = (ctx) => {
  return _.get(ctx.meta, 'role.permission') === '*'
}

const isManager = (ctx) => {
  return isAdmin(ctx) || _.get(ctx.meta, 'role.editUserprop')
}

const queryOnFilterCol = (role, entity, fields) => {
  const query = {}
  const filterCol = _.get(role, `filterCol.${entity}`, {})
  if (_.isPlainObject(filterCol)) {
    _.forOwn(filterCol, (v, k) => {
      if (!(fields || []).includes(k)) return
      if (_.isPlainObject(v)) {
        query[k] = v
      } else if (_.isString(v)) {
        if (v.substr(0, 4) === '$in:') {
          // filter from resource
          const parts = splitString(v, ':')
          const values = _.get(role, `resource.${parts[1]}`)
          if (_.isArray(values)) query[k] = { $in: values }
        }
      }
    })
  }
  return query
}

const buildApiFilter = (ctx, entity) => {
  let query = {}
  const svc = ctx.broker.getLocalService(entity)
  if (isSiteAware(ctx, svc.schema)) _.set(query, 'siteId', { $in: [0, ctx.meta.site.id] })
  if (!(isManager(ctx) && ctx.params._su)) {
    if (isUserAware(ctx, svc.schema)) _.set(query, 'userId', ctx.meta.user.id)
    const q = queryOnFilterCol(ctx.meta.role, entity, svc.settings.fields)
    query = _.merge(query, q)
  }
  if (isShareable(ctx, svc.schema)) {
    if (ctx.meta.user) {
      const share = { $or: [] }
      share.$or.push({ ownerId: ctx.meta.user.id, shared: 'NO' })
      share.$or.push({ shared: { $in: ['RA', 'WA'] } })
      if (!_.isEmpty(ctx.meta.role.team)) {
        share.$or.push({ shared: { $in: ['RT', 'WT'] }, ownerId: { $in: ctx.meta.role.team } })
      }
      query = { $and: [query, share] }
    } else {
      query.shared = { $in: ['RA', 'WA'] }
    }
  }

  let q
  if (ctx.params.query) {
    if (_.isPlainObject(ctx.params.query)) {
      q = ctx.params.query || {}
    } else {
      try {
        q = JSON.parse(ctx.params.query)
      } catch (err) {}
    }
  } else if (ctx.params.rsql) q = rsql(ctx.params.rsql)
  else if (ctx.params.nql) q = nql(ctx.params.nql).parse()

  let all
  if (!_.isEmpty(q) && !_.isEmpty(query)) all = { $and: [query, q] }
  else if (!_.isEmpty(q)) all = q
  else if (!_.isEmpty(query)) all = query
  return Promise.resolve(all)
}

const enableCache = ctx => {
  const cached = ctx.params._noCache !== true
  const isSd = ctx.action.service.settings.isSd
  return isSd ? false : cached
}

const getEntity = (ctx, params) => {
  const entity = params || _.get(ctx, 'params.entity')
  if (_.isEmpty(entity)) throw createError('Entity not provided', 400)
  const svcName = _.camelCase(_.kebabCase(entity))
  const svc = _.find(ctx.broker.services, { name: svcName })
  if (!svc) throw createError('Invalid entity', 400)
  return { svc, svcName }
}

const upload = (ctx, { destDir, fname }) => {
  if (!fname) fname = generateId()
  return new Promise((resolve, reject) => {
    if (!ctx.meta.user) throw createError('Unauthenticated', 401)
    fs.ensureDirSync(destDir)
    let ext = path.extname(fname)
    if (_.isEmpty(ext)) ext = mime.extension(ctx.meta.mimetype)
    else fname = path.basename(fname, ext)
    if (ext[0] !== '.') ext = '.' + ext
    const isImage = ['.jpg', '.png', '.jpeg'].includes(ext)
    const filePath = path.join(ctx.broker.sobr.dataDir, 'tmp', generateId() + ext)
    const filePathIman = path.join(ctx.broker.sobr.dataDir, 'tmp', generateId() + ext)
    const filePathNew = path.join(destDir, fname + ext)
    const f = fs.createWriteStream(filePath)
    f.on('close', () => {
      Promise.resolve()
        .then(() => {
          if (!isImage) return false
          let transform = (ctx.meta.$multipart || {}).transform
          if (_.isEmpty(transform)) return
          try {
            transform = JSON.parse(transform)
          } catch (err) {}
          if (!(_.isArray(transform) && transform.length > 0)) return false
          const iman = ctx.broker.getLocalService('imanTransform')
          if (!iman) return false
          return ctx.broker.call('imanTransform.mix', {
            source: filePath,
            tasks: transform,
            dest: filePathIman
          })
        })
        .then(result => {
          const src = result ? filePathIman : filePath
          fs.copyFileSync(src, filePathNew)
        })
        .then(() => {
          resolve({
            fileUploaded: path.basename(filePathNew),
            fileOriginal: ctx.meta.filename,
            fieldName: ctx.meta.fieldname,
            mimeType: ctx.meta.mimetype,
            encoding: ctx.meta.encoding
          })
        })
        .catch(err => {
          reject(err)
        })
    })

    ctx.params.on('error', err => {
      reject(err)
      f.destroy(err)
    })

    f.on('error', () => {
      fs.unlinkSync(filePath)
    })

    ctx.params.pipe(f)
  })
}

module.exports = {
  isUserAware,
  isSiteAware,
  isShareable,
  isAdmin,
  isManager,
  queryOnFilterCol,
  buildApiFilter,
  enableCache,
  getEntity,
  upload
}
