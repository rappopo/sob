/**
 * Stolen from: https://github.com/moleculerjs/moleculer-db/blob/master/packages/moleculer-db-adapter-sequelize/src/index.js
 * Adjusted to work with Rappopo Sob and comply to JS Standard
 */
const _ = require('lodash')
const { ServiceSchemaError } = require('moleculer').Errors
const Sequelize = require('sequelize')
const transformSort = require('./transform-sort')

const { Model, Op } = Sequelize

class SequelizeDbAdapter {
  constructor (...opts) {
    this.opts = opts
  }

  init (broker, service) {
    this.broker = broker
    this.service = service

    if (!this.service.schema.model) {
      throw new ServiceSchemaError('Missing `model` definition in schema of service!')
    }
  }

  connect () {
    const sequelizeInstance = this.opts[0]
    // if (sequelizeInstance && sequelizeInstance instanceof Sequelize) this.db = sequelizeInstance
    // else this.db = new Sequelize(...this.opts)
    this.db = sequelizeInstance

    return this.db.authenticate().then(() => {
      const modelDefinitionOrInstance = this.service.schema.model

      let noSync = false
      if (this.opts[0] && Object.prototype.hasOwnProperty.call(this.opts[0], 'noSync')) {
        noSync = !!this.opts[0].noSync
      } else if (this.opts[0] && Object.prototype.hasOwnProperty.call(this.opts[0], 'sync')) {
        noSync = !this.opts[0].sync.force
      } else if (this.opts[3] && Object.prototype.hasOwnProperty.call(this.opts[3], 'sync')) {
        noSync = !this.opts[3].sync.force
      } else if (this.opts[3]) {
        noSync = !!this.opts[3].noSync
      }

      let modelReadyPromise
      const isModelInstance = modelDefinitionOrInstance &&
        (Object.prototype.hasOwnProperty.call(modelDefinitionOrInstance, 'attributes') ||
        modelDefinitionOrInstance.prototype instanceof Model)
      if (isModelInstance) {
        this.model = modelDefinitionOrInstance
        modelReadyPromise = Promise.resolve()
      } else {
        this.model = this.db.define(modelDefinitionOrInstance.name, modelDefinitionOrInstance.define, modelDefinitionOrInstance.options)
        modelReadyPromise = noSync ? Promise.resolve(this.model) : this.model.sync()
      }
      this.service.model = this.model

      return modelReadyPromise.then(() => {
        this.service.logger.info('Sequelize adapter has connected successfully.')
      }).catch((e) => {
        return this.db.close()
          .finally(() => Promise.reject(e))
      })
    })
  }

  disconnect () {
    if (this.db) {
      return this.db.close()
    }
    return Promise.resolve()
  }

  find (filters) {
    return this.createCursor(filters)
  }

  findOne (query) {
    return this.model.findOne(query)
  }

  findById (_id) {
    return this.model.findByPk(_id)
  }

  findByIds (idList) {
    return this.model.findAll({
      where: {
        id: {
          [Op.in]: idList
        }
      }
    })
  }

  count (filters = {}) {
    return this.createCursor(filters, true)
  }

  insert (entity) {
    const opts = {}
    if (_.has(entity, 'createdAt')) opts.raw = true
    return this.model.create(entity, opts)
  }

  upsert (entity) {
    const opts = { hooks: false }
    return this.model.upsert(entity, opts)
  }

  insertMany (entities, opts = { returning: true }) {
    return this.model.bulkCreate(entities, opts)
  }

  updateMany (where, update) {
    return this.model.update(update, { where }).then(res => res[0])
  }

  updateById (_id, update) {
    return this.findById(_id).then(entity => {
      return entity && entity.update(update['$set'])
    })
  }

  removeMany (where) {
    return this.model.destroy({ where })
  }

  removeById (_id) {
    return this.findById(_id).then(entity => {
      return entity && entity.destroy().then(() => entity)
    })
  }

  clear () {
    return this.model.destroy({ where: {} })
  }

  entityToObject (entity) {
    return entity.get({ plain: true })
  }

  createCursor (params, isCounting) {
    if (!params) {
      if (isCounting) return this.model.count()
      return this.model.findAll()
    }

    const q = {
      where: {}
    }

    // Text search
    if (_.isString(params.search) && params.search !== '') {
      let fields = []
      if (params.searchFields) {
        fields = _.isString(params.searchFields) ? params.searchFields.split(' ') : params.searchFields
      }

      const searchConditions = fields.map(f => {
        return {
          [f]: {
            [Op.like]: '%' + params.search + '%'
          }
        }
      })

      if (params.query) {
        q.where[Op.and] = [
          params.query,
          { [Op.or]: searchConditions }
        ]
      } else {
        q.where[Op.or] = searchConditions
      }
    } else if (params.query) {
      Object.assign(q.where, params.query)
    }

    // Sort
    if (params.sort) {
      const sort = transformSort(params.sort)
      if (sort) q.order = sort
    }

    // Offset
    if (_.isNumber(params.offset) && params.offset > 0) q.offset = params.offset
    // Limit
    if (_.isNumber(params.limit) && params.limit > 0) q.limit = params.limit
    if (isCounting) return this.model.count(q)

    return this.model.findAll(q)
  }

  beforeSaveTransformID (entity, idField) {
    return entity
  }

  afterRetrieveTransformID (entity, idField) {
    return entity
  }
}

module.exports = SequelizeDbAdapter
