const { _, fs } = require('../helper')
const serveStatic = require('serve-static')
const cors = require('../../mw/cors')

module.exports = function (config, broker) {
  const routes = []
  if (_.isBoolean(config.web.public)) {
    config.web.public = {
      path: '/assets'
    }
  }
  const dir = `${broker.sobr.dataDir}/public`
  if (!fs.existsSync(dir)) fs.mkdirSync(dir)
  const p = serveStatic(dir, config.web.public.options)
  routes.push({
    path: config.web.public.path,
    cors: config.web.public.cors,
    use: [p],
    onError (req, res, err) {
      res.setHeader('Content-Type', 'text/plain')
      res.writeHead(err.code)
      res.end()
    }
  })

  // public in modules
  _.each(broker.services, s => {
    if (!s.settings.sob) return
    if (s.settings.sob.name === 'app') return
    const dir = `${s.settings.sob.pkgDir}/data/public`
    if (!fs.existsSync(dir)) return
    const path = config.web.public.path + `/${s.settings.sob.name}`
    if (_.map(routes, 'path').includes(path)) return
    const p = serveStatic(dir, config.web.public.options)
    routes.push({
      path,
      cors: config.web.api.cors,
      use: [cors(config), p],
      onError (req, res, err) {
        res.setHeader('Content-Type', 'text/plain')
        res.writeHead(err.code)
        res.end()
      }
    })
  })
  return routes
}
