const _ = require('lodash')

const apiMethods = ['list', 'get', 'create', 'update', 'remove', 'options']

const isUpperCase = val => {
  const uval = val.toUpperCase()
  return val === uval
}

const pathBuilder = (name, builder) => {
  if (name.substr(0, 3) === 'app' && isUpperCase(name.substr(3, 1))) name = _.camelCase(name.substr(3))
  let result = name
  const parts = name.split('/')
  const dashed = _.kebabCase(parts[0])
  parts.shift()
  switch (builder) {
    case 'dash': result = dashed; break
    case 'slash': result = dashed.replace(/-/g, '/'); break
    case 'under': result = dashed.replace(/-/g, '_'); break
  }
  if (parts.length > 0) result += '/' + parts.join('/')
  return result
}

const pathBuilderMany = (obj = {}, builder) => {
  const result = {}
  _.forOwn(obj, (v, k) => {
    const parts = k.replace(/\s+/g, ' ').split(' ')
    parts[1] = pathBuilder(parts[1], builder)
    result[parts.join(' ')] = v
  })
  return result
}

module.exports = {
  apiMethods,
  pathBuilder,
  pathBuilderMany
}
