const { _, createError } = require('../helper')
const { apiMethods } = require('./common')

module.exports = async function (ctx, route, req, res) {
  const svc = req.$action.service
  if (svc.settings.sd) return
  let entity
  if (req.$action.name === 'coreStream.serve') {
    entity = _.get(ctx, 'params.req.$params.entity')
    if (entity) {
      const parts = _.kebabCase(entity).split('-')
      if (parts[1] === 'sd') return
      let skip = false
      _.each(this.broker.services, s => {
        if (s.name === entity && _.get(s, 'settings.webStream.anonymous')) {
          skip = true
          return false
        }
      })
      if (skip) return
    }
  }
  let auth
  const getRole = _.get(this.broker, 'sobr.sob.core.config.role.byUser', '')
  const checker = _.get(this.broker, 'sobr.sob.core.config.role.checker', '')
  if (!_.isEmpty(req.query.token)) auth = ['Bearer', req.query.token]
  else auth = _.without((req.headers['authorization'] || '').split(' '), '', null, undefined)
  if (_.isEmpty(auth)) {
    const exclApi = _.map(_.get(svc, 'settings.webApi.anonymous', []), a => {
      const action = apiMethods.includes(a) ? _.camelCase(`api ${a}`) : a
      return `${svc.name}.${action}`
    })
    const exclRoot = _.map(_.get(svc, 'settings.webRoot.anonymous', []), a => {
      return `${svc.name}.${a}`
    })
    const excluded = _.concat(exclApi, exclRoot)
    if (excluded.includes(req.$action.name) || entity) {
      if (_.isEmpty(getRole)) return
      if (_.isEmpty(checker)) throw createError('No method to check role', 500)
      const role = await this.broker.call(getRole, { site: ctx.meta.site })
      await this.broker.call(checker, { action: req.$action, route, site: ctx.meta.site, entity, role })
      ctx.meta.role = role
      return
    }
    throw createError('Authorization info not found', 401)
  }
  if (!this.broker.sobr.sob.core.config.web.api.supportedAuthMethods.includes(auth[0])) throw createError('Authorization method not supported', 401)
  let user
  if (auth[0] === 'JWT') {
    user = await ctx.call('coreUser.verifyJwt', { token: auth[1] })
  } else if (auth[0] === 'Bearer') {
    user = await ctx.call('coreUser.verifyBearer', { token: auth[1] })
  } else if (auth[0] === 'Basic') {
    const info = Buffer.from(auth[1], 'base64').toString().split(':')
    if (info.length !== 2) throw createError('Invalid authorization info', 401)
    user = await ctx.call('coreUser.verifyBasic', { username: info[0], password: info[1] })
  }
  if (!user) throw createError('Invalid login', 401)
  if (user.status !== 'ENABLED') throw createError('Invalid user or user is currently disabled', 401)
  // now check role
  if (_.isEmpty(getRole)) {
    ctx.meta.user = user
    return
  }
  if (_.isEmpty(checker)) throw createError('No method to check role', 500)
  const role = await this.broker.call(getRole, { user, site: ctx.meta.site })
  await this.broker.call(checker, { user, action: req.$action, route, site: ctx.meta.site, entity, role })
  ctx.meta.user = user
  ctx.meta.role = role
}
