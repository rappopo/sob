const onWebCall = require('./on-web-call')
const { _, mergeClone } = require('../helper')
const { pathBuilder, pathBuilderMany } = require('./common')

const handler = function (s, broker, config, aliases) {
  const sobs = _.keys(s.broker.sobr.sob)
  const methods = ['webGet', 'webPost']
  const sobName = (s.settings.sob || {}).name
  if (!sobs.includes(sobName)) return aliases
  const webRoot = _.get(broker.sobr, `sob.${sobName}.config.web.root`, { alias: {} })
  webRoot.middleware = webRoot.middleware || []
  s.settings.webRoot = _.merge({ readonly: false, middleware: [] }, s.settings.webRoot)
  const custom = pathBuilderMany(webRoot.alias, config.web.root.pathBuilder)
  aliases = mergeClone(aliases, custom)
  _.forOwn(aliases, (v, k) => {
    aliases[k] = _.concat(webRoot.middleware, v)
  })
  let routePath = pathBuilder(s.name, config.web.root.pathBuilder)
  let route = _.get(config, `web.root.route.${s.name}`)
  if (_.isBoolean(route) && !route) return aliases
  if (_.isUndefined(route) || (_.isArray(route) && route.length === 0)) route = methods
  if (_.isPlainObject(route)) {
    if (!_.isEmpty(route.path)) routePath = route.path
    if (!_.isEmpty(route.methods)) route = route.methods
    else route = methods
  }
  if (s.actions.webGet && route.includes('webGet')) aliases[`GET ${routePath}`] = _.concat(webRoot.middleware, s.settings.webRoot.middleware.request || [], s.name + '.webGet')
  if (s.settings.webRoot.readonly) return aliases
  if (s.actions.webPost && route.includes('webPost')) aliases[`POST ${routePath}`] = _.concat(webRoot.middleware, s.settings.webRoot.middleware.submit || [], s.name + '.webPost')
  const alias = _.get(s.settings, 'webRoot.alias', {})
  if (!_.isEmpty(alias)) {
    _.forOwn(alias, (v, k) => {
      const [method, path] = k.split(' ')
      if (!['GET', 'POST'].includes(method)) return
      aliases[`${method} ${routePath}${path}`] = _.concat(webRoot.middleware, s.name + '.' + v)
    })
  }
  return aliases
}

module.exports = function (config, broker) {
  let aliases = pathBuilderMany({
    'GET coreMyAvatar': 'coreUser.getMyAvatar',
    'GET coreUserAvatar/:id': 'coreUser.getAvatar',
    'GET coreSiteLogo': 'coreSite.getLogo'
  }, config.web.root.pathBuilder)
  _.each(broker.services, s => {
    aliases = handler(s, broker, config, aliases)
  })

  return [{
    path: config.web.root.path,
    cors: config.web.root.cors,
    authorization: true,
    whitelist: ['**'],
    mappingPolicy: 'restrict',
    bodyParsers: {
      json: true,
      urlencoded: { extended: true }
    },
    aliases,
    async onBeforeCall (ctx, route, req, res) {
      return onWebCall(ctx, route, req, res)
    }
  }]
}
