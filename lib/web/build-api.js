const onWebCall = require('./on-web-call')
const onAfterCall = require('./on-after-call')
const { _, mergeClone } = require('../helper')
const { apiMethods, pathBuilder, pathBuilderMany } = require('./common')

const handler = function (s, broker, config, aliases) {
  const methods = apiMethods
  const sobs = _.keys(s.broker.sobr.sob)
  const sobName = (s.settings.sob || {}).name
  if (!sobs.includes(sobName)) return aliases
  // sob's global
  const webApi = _.get(broker.sobr, `sob.${sobName}.config.web.api`, { alias: { sd: true, custom: {} } })
  webApi.middleware = webApi.middleware || []
  aliases = mergeClone(aliases, pathBuilderMany(webApi.alias.custom, config.web.api.pathBuilder))
  const custom = {}
  _.forOwn(_.get(s.settings, 'webApi.alias', {}), (v, k) => {
    const parts = k.split(' ')
    const key = parts.length === 1 ? `${k} ${s.name}` : `${parts[0]} ${s.name}${parts[1]}`
    custom[key] = `${s.name}.${v}`
  })

  aliases = mergeClone(aliases, pathBuilderMany(custom, config.web.api.pathBuilder))
  _.forOwn(aliases, (v, k) => {
    aliases[k] = _.concat(webApi.middleware, v)
  })
  const isSd = s.name.substr(0, 2) === 'sd' || (sobName + 'Sd') === s.name.substr(0, (sobName + 'Sd').length)
  const allowSd = webApi.alias.sd
  if (isSd && !allowSd) return aliases

  let routePath = pathBuilder(s.name, config.web.api.pathBuilder)
  let route = _.get(config, `web.api.route.${s.name}`)
  if (_.isBoolean(route) && !route) return aliases
  if (_.isUndefined(route) || (_.isArray(route) && route.length === 0)) route = methods
  if (_.isPlainObject(route)) {
    if (!_.isEmpty(route.path)) routePath = route.path
    if (!_.isEmpty(route.methods)) route = route.methods
    else route = methods
  }
  s.settings.webApi = s.settings.webApi || {}
  let disabled = _.get(s.settings, 'webApi.disabled', [])
  if (disabled === 'all') disabled = ['list', 'get', 'create', 'update', 'remove']
  if (s.actions.list && route.includes('list') && !disabled.includes('list')) aliases[`GET ${routePath}`] = _.concat(webApi.middleware, _.get(s.settings, 'webApi.middleware.list', []), s.name + '.apiList')
  if (s.actions.get && route.includes('get') && !disabled.includes('get')) aliases[`GET ${routePath}/:id`] = _.concat(webApi.middleware, _.get(s.settings, 'webApi.middleware.get', []), s.name + '.apiGet')
  if (isSd || _.get(s.settings, 'webApi.readonly')) return aliases
  if (s.actions.create && route.includes('create') && !disabled.includes('create')) aliases[`POST ${routePath}`] = _.concat(webApi.middleware, _.get(s.settings, 'webApi.middleware.create', []) || [], s.name + '.apiCreate')
  if (s.actions.update && route.includes('update') && !disabled.includes('update')) aliases[`PUT ${routePath}/:id`] = _.concat(webApi.middleware, _.get(s.settings, 'webApi.middleware.update', []) || [], s.name + '.apiUpdate')
  if (s.actions.remove && route.includes('remove') && !disabled.includes('remove')) aliases[`DELETE ${routePath}/:id`] = _.concat(webApi.middleware, _.get(s.settings, 'webApi.middleware.remove', []), s.name + '.apiRemove')
  return aliases
}

module.exports = function (config, broker) {
  let aliases = pathBuilderMany({
    'POST coreTokenJwt': 'coreUser.createJwt',
    'GET coreTokenBearer': 'coreUser.getBearer',
    'POST coreTokenBearer': 'coreUser.createBearer',
    'GET coreMyRole': 'coreUser.getMyRole',
    'GET coreMyProfile': 'coreUser.getMyProfile',
    'PUT coreMyProfile': 'coreUser.updateMyProfile',
    'PUT coreMyPassword': 'coreUser.changePassword',
    'POST coreUserRegister': 'coreUser.register',
    'POST coreUserActivate': 'coreUser.activate',
    'DELETE coreMyAvatar': 'coreUser.removeAvatar',
    'DELETE coreSiteLogo': 'coreSite.removeLogo'
  }, config.web.api.pathBuilder)

  _.each(broker.services, s => {
    aliases = handler(s, broker, config, aliases)
  })

  const routes = []
  if (!_.isEmpty(aliases)) {
    routes.push({
      path: config.web.api.path,
      cors: config.web.api.cors,
      authorization: true,
      whitelist: ['**'],
      mappingPolicy: 'restrict',
      bodyParsers: {
        json: true,
        urlencoded: { extended: true }
      },
      aliases,
      async onBeforeCall (ctx, route, req, res) {
        return onWebCall(ctx, route, req, res)
      },
      onAfterCall
    })
  }

  if (config.web.tenancy) {
    routes.push({
      path: config.web.api.pathSite || '/api/tenancy',
      cors: config.web.api.cors,
      authorization: true,
      whitelist: ['**'],
      mappingPolicy: 'restrict',
      bodyParsers: {
        json: true,
        urlencoded: { extended: true }
      },
      aliases: pathBuilderMany({
        'REST user': 'coreUser',
        'REST site': 'coreSite'
      }, config.web.api.pathBuilder),
      async onBeforeCall (ctx, route, req, res) {
        await onWebCall(ctx, route, req, res)
        ctx.meta.tenancy = true
      },
      onAfterCall
    })
  }
  return routes
}
