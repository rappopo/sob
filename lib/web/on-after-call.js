const _ = require('lodash')

const setContentRange = function (ctx, route, req, res, data) {
  if (data && _.isArray(data.rows)) {
    const start = (data.page * data.pageSize) - data.pageSize
    const end = start + data.rows.length
    const action = req.$action.name.split('.')[0]
    ctx.meta.$responseHeaders = {
      'Content-Range': `${action} ${start}-${end}/${data.total}`
    }
  }
}

const stripFields = function (ctx, route, req, res, data) {
  if (!data) return data
  if (req.$action.name.split('.')[0] === 'user') {
    const omitted = ['password', 'keyX', 'keyD']
    if (_.isArray(data.rows)) {
      _.each(data.rows, (d, i) => {
        data.rows[i] = _.omit(d, omitted)
      })
    } else {
      data = _.omit(data, omitted)
    }
  }
  return data
}

module.exports = async function (ctx, route, req, res, data) {
  data = stripFields(ctx, route, req, res, data)
  setContentRange(ctx, route, req, res, data)
  return data
}
