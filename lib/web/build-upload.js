const onWebCall = require('./on-web-call')
const { _, mergeClone, fs } = require('../helper')
const { pathBuilder, pathBuilderMany } = require('./common')
const path = require('path')

const handler = function (s, broker, config, aliases) {
  const sobs = _.keys(s.broker.sobr.sob)
  const methods = ['upload']
  const sobName = (s.settings.sob || {}).name
  if (!sobs.includes(sobName)) return aliases
  const upload = _.get(broker.sobr, `sob.${sobName}.config.web.upload`, { alias: {} })
  s.settings.upload = s.settings.upload || {}
  const custom = pathBuilderMany(upload.alias, config.web.upload.pathBuilder)
  aliases = mergeClone(aliases, custom)
  let routePath = pathBuilder(s.name, config.web.upload.pathBuilder)
  let route = _.get(config, `web.upload.route.${s.name}`)
  if (_.isBoolean(route) && !route) return aliases
  if (_.isUndefined(route) || (_.isArray(route) && route.length === 0)) route = methods
  if (_.isPlainObject(route)) {
    if (!_.isEmpty(route.path)) routePath = route.path
    if (!_.isEmpty(route.methods)) route = route.methods
    else route = methods
  }

  const params = _.get(s.settings, 'upload.params', {})
  const alias = _.get(s.settings, 'upload.alias', {})
  if (s.actions.upload && route.includes('upload')) alias[''] = 'upload'
  if (!_.isEmpty(alias) && route.includes('upload')) {
    _.forOwn(alias, (v, k) => {
      aliases[`POST ${routePath}${k}`] = {
        type: params.type || 'multipart',
        busboyConfig: params.busboyConfig || { limits: { files: 1 } },
        action: s.name + '.' + v
      }
    })
  }
  return aliases
}

module.exports = function (config, broker) {
  let aliases = pathBuilderMany({
    'POST coreMyAvatar': 'multipart:coreUser.uploadAvatar',
    'POST coreSiteLogo': 'multipart:coreSite.uploadLogo'
  }, config.web.upload.pathBuilder)

  _.each(broker.services, s => {
    aliases = handler(s, broker, config, aliases)
  })

  if (_.isEmpty(aliases)) return []
  const dir = path.join(broker.sobr.dataDir, 'upload')
  fs.ensureDirSync(dir)
  return [{
    path: config.web.upload.path,
    cors: config.web.upload.cors,
    authorization: true,
    whitelist: ['**'],
    mappingPolicy: 'restrict',
    bodyParsers: {
      json: false,
      urlencoded: false
    },
    aliases,
    busboyConfig: config.web.upload.busboyConfig,
    async onBeforeCall (ctx, route, req, res) {
      return onWebCall(ctx, route, req, res)
    }
  }]
}
