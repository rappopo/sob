const { _, createError } = require('../helper')

module.exports = async function (ctx, route, req, res) {
  const proxyHost = req.headers['x-forwarded-host']
  const host = (proxyHost || req.headers.host).split(':')[0]
  const tenancy = _.get(ctx.broker, 'sobr.sob.core.config.web.tenancy')
  const query = tenancy ? { domain: host, status: 'ENABLED' } : { id: 1 }
  const sites = await ctx.call('coreSite.find', { query, limit: 1 })
  if (sites.length === 0) throw createError('Invalid domain or domain is currently disabled', 500)
  ctx.meta.site = sites[0]
  if (!tenancy) delete ctx.meta.site.status
}
