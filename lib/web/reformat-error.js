const _ = require('lodash')

module.exports = function (err) {
  let errors
  if (_.isPlainObject(err.data)) errors = err.data
  else if (_.isArray(err.data)) {
    errors = {}
    _.each(err.data, d => {
      if (errors[d.field]) errors[d.field] += (',' + d.type)
      else errors[d.field] = d.type
    })
  }
  return { message: err.message, errors, code: err.code }
}
