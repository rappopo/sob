const util = require('util')
const { DateTime, Duration } = require('luxon')
const qs = require('query-string')
const axios = require('axios')
const crypto = require('crypto')
const scramjet = require('scramjet')
const S = require('string')
const parse = require('parse-duration')
const { DataStream, StringStream } = scramjet
const path = require('path')
const fs = require('fs-extra')
const yaml = require('js-yaml')
const { getInstalledPathSync } = require('get-installed-path')
const fg = require('fast-glob')
const argv = require('minimist')(process.argv.slice(2))
const Sequelize = require('sequelize')
const matcher = require('matcher')
const _ = require('lodash')
const sequelizeStream = require('../lib/sequelize-stream')
const JSONStream = require('JSONStream')
const kleur = require('kleur')
const rmfr = require('rmfr')
const Validator = require('fastest-validator')
const cliProgress = require('cli-progress')
const paginateArray = require('paginate-array')
const mingo = require('mingo')
const compressing = require('compressing')
const mime = require('mime-types')
const { customAlphabet } = require('nanoid')

const validator = new Validator()
const loggerName = '[Sob]'

const dump = (item, exit) => {
  console.log(util.inspect(item, { showHidden: false, depth: null }))
  if (exit) process.exit()
}

const isSet = value => {
  return !(value === null || value === undefined)
}

const logger = (options = {}) => {
  return {
    info (message) {
      console.log(kleur.grey(options.name || loggerName), kleur.green().bold(message))
    },
    warn (message) {
      console.log(kleur.grey(options.name || loggerName), kleur.yellow().bold(message))
    },
    error (err, exitOnError) {
      if (err instanceof Error) console.error(kleur.grey(options.name || loggerName), kleur.red().bold(err.message), err)
      else console.error(kleur.grey(options.name || loggerName), kleur.red().bold(err))
      if (exitOnError) process.exit(1)
    }
  }
}

const mergeClone = (source = {}, item = {}) => {
  return _.merge(_.cloneDeep(source), _.cloneDeep(item))
}

const toUnixPath = text => {
  return (text || '').replace(/\\/g, '/')
}

const getSobConfig = (name, sobr) => {
  return _.get(sobr, `sob.${name}.config`, {})
}

const getGlobalModPath = () => {
  const { execSync } = require('child_process')
  return execSync('npm root -g').toString().trim()
}

const createProgressBar = (options = {}) => {
  const bar = new cliProgress.SingleBar({
    format: `${kleur.grey(options.loggerName || loggerName)} ${kleur.yellow().bold('{bar} {percentage}% | {value}/{total}')} ${kleur.green().bold(options.labelChunks || 'Records')}`,
    barCompleteChar: '\u2588',
    barIncompleteChar: '\u2591',
    hideCursor: true
  })
  return bar
}

const wait = ms => new Promise(resolve => setTimeout(resolve, ms))
const hash = (text, type = 'md5', digest = 'hex') => crypto.createHash(type).update(text).digest(digest)

const getPkgPath = (pkg, unixPath = true) => {
  let dir
  try {
    dir = getInstalledPathSync(pkg, { local: true })
  } catch (err) {}
  if (!dir) {
    try {
      dir = getInstalledPathSync(pkg)
    } catch (err) {}
  }
  if (dir && unixPath) dir = toUnixPath(dir)
  return dir
}

const age = (dt1, dt2, as = 'ms') => {
  if (!dt1) dt1 = DateTime.local()
  if (!dt2) dt2 = DateTime.local()
  const diff = Math.abs(dt1.diff(dt2).as('seconds'))
  const obj = secondsToTime(diff)
  _.forOwn(obj, (v, k) => {
    obj[k] = _.padStart(v, 2, '0')
  })
  let out = diff
  switch (as) {
    case 'hms': out = `${obj.h}:${obj.m}:${obj.s}`; break
    case 'ms': out = `${obj.m}:${obj.s}`; break
  }
  return out
}

const nix = val => {}

const secondsToTime = secs => {
  var hours = Math.floor(secs / (60 * 60))
  var divMin = secs % (60 * 60)
  var minutes = Math.floor(divMin / 60)

  var divSec = divMin % 60
  var seconds = Math.ceil(divSec)

  return {
    h: hours,
    m: minutes,
    s: seconds
  }
}

const parseDuration = (time, format = 'ms') => {
  const asDuration = format === true
  if (asDuration) format = 'ms'
  if (_.isNumber(time)) time = time + format
  const parsed = parse(time, format)
  return asDuration ? Duration.fromMillis(parsed) : parsed
}

const fetch = (url, opts = {}) => {
  return new Promise((resolve, reject) => {
    opts.data = opts.data || {}
    const opt = mergeClone(_.omit(opts, ['method', 'data']), {
      url,
      method: opts.method || 'GET',
      cancelToken: opts.cancelToken
    })
    if (opts.responseType) opt.responseType = opts.responseType
    if (['POST', 'PUT', 'PATCH'].includes(opt.method)) opt.data = opts.data || {}
    else if (!_.isEmpty(opts.data)) opt.params = mergeClone(opt.params, opts.data)
    let endResult = []
    axios(opt).then(resp => {
      endResult = resp.data
      resolve(endResult)
    }).catch(err => {
      if (axios.isCancel(err)) return reject(err)
      if (err.response) return reject(err.response.data)
      reject(err)
    })
  })
}

const readConfig = async (fname, def = {}, modifier) => {
  const dir = path.dirname(fname)
  const ext = path.extname(fname)
  const base = path.basename(fname, ext)
  let config = def
  _.each(['js', 'json', 'yaml', 'yml'], x => {
    const file = `${dir}/${base}.${x}`
    if (!fs.existsSync(file)) return
    if (['yaml', 'yml'].includes(x)) {
      config = yaml.safeLoad(fs.readFileSync(file))
    } else {
      config = require(file)
    }
    return false
  })
  if (_.isFunction(config)) config = await config()
  if (_.isFunction(modifier)) config = await modifier(config)
  return config
}

const resolveFixture = async (sob, sobr, executor, modifier) => {
  const oldBuiltin = await readConfig(`${sob.pkgDir}/data/built-in/${sob.basename}.json`, [])
  const newBuiltin = await readConfig(`${sobr.dataDir}/override/built-in/${sob.service}.json`, [])
  let fixtures = _.merge(oldBuiltin, newBuiltin)
  _.each(fixtures, (f, i) => {
    fixtures[i].builtIn = true
  })
  const newFixtures = await readConfig(`${sobr.dataDir}/fixture/${sob.service}.json`, [])
  fixtures = _.concat(fixtures, newFixtures)
  if (modifier) {
    for (let i = 0; i < fixtures.length; i++) {
      fixtures[i] = await modifier(fixtures[i])
    }
  }
  await executor(fixtures)
  return fixtures
}

const cleanArray = arr => {
  return _.without(arr, '', null, undefined)
}

const splitString = (text, splitter = ',', trimmed) => {
  if (!isSet(trimmed)) trimmed = true
  const arr = _.map((text || '').split(splitter), i => {
    return trimmed ? _.trim(i) : i
  })
  return cleanArray(arr)
}

const matcherFromCli = async (flag, sobr, findSob, options = {}) => {
  let models = argv[flag]
  const log = logger(options.logger || {})
  if (_.isBoolean(models) && models) {
    log.error('You need to enter at least an action/model first!')
    return false
  }
  if (_.isString(models)) models = models.split(',')
  let input = []
  _.each(models, m => {
    const items = m.split(',')
    input = _.concat(input, items)
  })
  if (sobr.init.modules.length === 0) {
    log.info('No module loaded!')
    return false
  }
  const results = []
  for (let j = 0; j < sobr.init.modules.length; j++) {
    const m = sobr.init.modules[j]
    const dir = m === 'app' ? sobr.sob.app.pkgDir : await getPkgPath(m)
    const files = await fg(dir + '/svc/**/*.js')
    const sob = findSob(m, sobr)
    for (let i = 0; i < files.length; i++) {
      const nsob = _.cloneDeep(_.pick(sob, ['name', 'pkg', 'pkgDir', 'config']))
      const basename = files[i].replace(dir + '/svc/', '').replace('.js', '')
      nsob.basename = basename
      nsob.service = _.camelCase(`${sob.name} ${basename}`)
      results.push({ name: _.camelCase(sob.name + ' ' + basename), file: files[i], sob: nsob, basename })
    }
  }
  const match = matcher(_.map(results, 'name'), input)
  if (match.length === 0) {
    log.error('No matching model found!')
    return false
  }
  return _.filter(results, a => match.includes(a.name))
}

/**
 * Persist model's data to file
 *
 * @param {string} model - Sequelize model with stream attached
 * @param {string} dest - File name to save
 * @param {string} jsonLine - If true, file is written as JSON line format, otherwise in full JSON
 */

const persistModel = (model, dest, opts = {}) => {
  opts.batchSize = opts.batchSize || 100
  opts.limit = opts.limit || 100000
  if (opts.limit <= 0) delete opts.limit
  return new Promise((resolve, reject) => {
    const reader = model.findAllWithStream(_.omit(opts, ['format']))
    const writer = fs.createWriteStream(`${dest}`)
    let count = 0
    const format = opts.format || 'json'
    reader.on('error', reject)
    writer.on('error', reject)
    writer.on('finish', () => {
      if (format === 'json') fs.appendFileSync(dest, ']')
      resolve(count)
    })
    if (format === 'json') writer.write('[')
    let first = true
    DataStream
      .from(reader)
      .map(item => {
        item = JSON.parse(item.toString())
        const texted = _.map(item, i => JSON.stringify(i))
        count = count + item.length
        let result = ''
        if (format !== 'json') {
          result = texted.join('\n') + '\n'
        } else {
          if (!first) result = ','
          result += texted.join(',')
        }
        first = false
        return result
      })
      .pipe(writer)
  })
}

/**
 * Load model data from file
 *
 * @param {*} model - Sequelize model
 * @param {*} src - File to be loaded
 * @param {*} jsonLine - Is the content in JSON line format?
 */

const loadModel = (model, src, opts = {}) => {
  const log = logger(opts.logger || {})
  const format = opts.format || 'json'
  return new Promise((resolve, reject) => {
    if (!fs.existsSync(src)) return resolve(0)
    const reader = fs.createReadStream(src)
    let count = 0
    reader.on('error', reject)
    let stream
    if (format !== 'json') {
      stream = StringStream.from(reader).lines()
    } else {
      stream = DataStream.pipeline(
        reader,
        JSONStream.parse('*')
      )
    }
    stream
      .batch(opts.batchNum || 1000)
      .map(async items => {
        if (_.isEmpty(items)) return
        const records = []
        for (let i = 0; i < items.length; i++) {
          items[i] = _.trim(items[i])
          if (_.isEmpty(items[i])) continue
          try {
            if (_.isString(items[i])) items[i] = JSON.parse(items[i])
            if (_.isEmpty(items[i])) continue
            count++
            records.push(items[i])
          } catch (err) {}
        }
        try {
          await model.bulkCreate(records, {
            hooks: false,
            ignoreDuplicates: false,
            validate: false,
            updateOnDuplicate: _.keys(model.rawAttributes)
          })
        } catch (err) {
          log.error(`Load error: ${err.message}`)
        }
      })
      .run()
      .then(() => {
        resolve(count)
      })
      .catch(reject)
  })
}

const createError = (msg, code, data) => {
  const err = new Error(msg)
  err.code = isSet(code) ? code : 500
  if (data) err.data = data
  return err
}

const generateId = (pattern, length, instance) => {
  pattern = pattern || 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'
  length = length || 10
  const nid = customAlphabet(pattern, length)
  return instance ? nid : nid()
}

module.exports = {
  _,
  rmfr,
  qs,
  fs,
  fg,
  S,
  mingo,
  validator,
  logger,
  scramjet,
  Sequelize,
  DateTime,
  Duration,
  axios,
  yaml,
  matcher,
  JSONStream,
  compressing,
  sequelizeStream,
  mime,
  paginateArray,
  dump,
  generateId,
  mergeClone,
  toUnixPath,
  wait,
  getSobConfig,
  hash,
  age,
  cleanArray,
  splitString,
  secondsToTime,
  fetch,
  resolveFixture,
  readConfig,
  matcherFromCli,
  persistModel,
  loadModel,
  parseDuration,
  getGlobalModPath,
  createProgressBar,
  getPkgPath,
  createError,
  isSet,
  nix
}
