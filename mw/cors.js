// https://github.com/moleculerjs/moleculer-web/blob/d181713e353ad90b08893b5c573ab19195645f93/src/index.js#L991

const { ForbiddenError, ERR_ORIGIN_NOT_ALLOWED } = require('moleculer')
const _ = require('lodash')

module.exports = function (config) {
  const cors = config.web.api.cors
  return function (req, res, next) {
    const origin = req.headers['origin']
    // if (!origin) return

    if (!cors.origin || cors.origin === '*') {
      res.setHeader('Access-Control-Allow-Origin', '*')
    } else if (this.checkOrigin(origin, cors.origin)) {
      res.setHeader('Access-Control-Allow-Origin', origin)
      res.setHeader('Vary', 'Origin')
    } else {
      throw new ForbiddenError(ERR_ORIGIN_NOT_ALLOWED)
    }
    if (cors.credentials === true) {
      res.setHeader('Access-Control-Allow-Credentials', 'true')
    }

    if (_.isString(cors.exposedHeaders)) {
      res.setHeader('Access-Control-Expose-Headers', cors.exposedHeaders)
    } else if (Array.isArray(cors.exposedHeaders)) {
      res.setHeader('Access-Control-Expose-Headers', cors.exposedHeaders.join(', '))
    }

    if (req.method === 'OPTIONS') {
      if (_.isString(cors.allowedHeaders)) {
        res.setHeader('Access-Control-Allow-Headers', cors.allowedHeaders)
      } else if (Array.isArray(cors.allowedHeaders)) {
        res.setHeader('Access-Control-Allow-Headers', cors.allowedHeaders.join(', '))
      } else {
        const allowedHeaders = req.headers['access-control-request-headers']
        if (allowedHeaders) {
          res.setHeader('Vary', 'Access-Control-Request-Headers')
          res.setHeader('Access-Control-Allow-Headers', allowedHeaders)
        }
      }

      if (_.isString(cors.methods)) {
        res.setHeader('Access-Control-Allow-Methods', cors.methods)
      } else if (Array.isArray(cors.methods)) {
        res.setHeader('Access-Control-Allow-Methods', cors.methods.join(', '))
      }

      if (cors.maxAge) {
        res.setHeader('Access-Control-Max-Age', cors.maxAge.toString())
      }
    }
    next()
  }
}
