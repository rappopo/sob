module.exports = {
  name: 'singleService',
  serviceStopped (svc) {
    return new Promise((resolve, reject) => {
      if (!svc.unlockFn) return resolve()
      svc.unlockFn().then(resolve).catch(err => {
        svc.logger.error(err.message)
      })
    })
  }
}
